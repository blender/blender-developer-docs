# Google Summer of Code - Application Template

Students will have to submit a proposal on the GSoC website based on the
template below. The proposal must be submitted as a final PDF before the application deadline.

You can submit a draft proposal early on at our [devtalk forum](https://devtalk.blender.org/c/contributing-to-blender/summer-of-code/15), get feedback and iterate on
it.

Be sure to read Google's [guide to writing a
proposal](https://google.github.io/gsocguides/student/writing-a-proposal)
and our [getting started page](getting_started.md).

## Template

### Project Title

: Please provide the title of your proposed project.

### Name

: Please provide your full name.

### Contact

: Where can we contact you?

  Add your email and [chat.blender.org](https://chat.blender.org) nickname.
  You can also provide additional info like your social media handles and website.

  !!! Tip
        You can leave out private information like your mail address when posting a draft in public.

### Synopsis

: A short description of your project idea.

### Benefits

: Describe how your project will improve Blender.

  How will it benefit the artists using Blender for 3D content creation? Will it
  be an aid for future Blender development?

### Deliverables

: Provide a summary of the final output or results of your project in
  terms that are understandable by artists and user the community.

  How does it integrate in Blender and how does it cooperate with the rest of
  Blender's features?
  Note that end-user documentation should be one of the deliverables.

### Project Details

: Elaborate your project idea. Here is the place to add technical
  explanations, architecture diagrams or UI/UX mockups.
  Make sure that your vision is clear and avoid fuzzy promises that you aren't
  sure about. If you have doubts, research and ask for feedback and
  clarifications beforehand.

### Project Schedule

: Specify how long the project will take and when you can you start.

  Add a timeline with the breakdown of the work. Include milestones with
  deliverables.

  !!! Tip
      Leave at least a week at the end to cope with delays.

  Please note any time that you will be unavailable during the project period.
  Will there be school exams or other events that conflict with the schedule?

### Bio

: Tell us a little bit about yourself. Are you studying, working and where are you from?
  What activities do you enjoy?

  What is your experience using Blender or other computer graphics programs?
  What code development projects have you participated in so far? What is your
  level of proficiency with C, C++, Python and programming in general?

  What makes you the best person to work on this project? If you have any
  history submitting bug fixes or patches to our tracker, please indicate what
  you have done.

## Examples

Browse the accepted [projects from the previous years](index.md).
