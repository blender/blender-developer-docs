# Renderdoc

Renderdoc is a widely used open source GPU debugger. Blender has several options
to benefit using renderdoc for debugging.

Renderdoc capturing requires compiling Blender with `WITH_RENDERDOC=On` and launching Blender
with the `--debug-gpu-renderdoc` command line parameter.

## Frame Capturing

1. Launch Blender from Renderdoc using the `--debug-gpu-renderdoc` command line parameter.
2. Press the `Play Animation` button to ensure the viewport is being rendered constantly.
3. Press the `PrintScreen` key to capture a frame.

![renderdoc-launch-settings.png](../../../images/renderdoc-launch-settings.png)

## Triggering captures from code

You can also start and stop a renderdoc capture from within Blenders' source code.

1. Add `GPU_debug_capture_begin`/`GPU_debug_capture_end` around the code you want to capture.
2. Launch Blender from Renderdoc using the `--debug-gpu-renderdoc` command line parameter.
3. Every time the `GPU_debug_capture_begin/end` pair is reached it will automatically record
   a capture.

### Example
In `view3d_draw_view` the viewport drawing events can be captured by surrounding 
`DRW_draw_view` with the GPU_debug_capture_begin/end function calls.

```cpp
GPU_debug_capture_begin(__func__);
DRW_draw_view(C);
GPU_debug_capture_end();
```

> NOTE: it is not allowed to land calls to `GPU_debug_capture_begin/end` in main as
> they are considered to be debug code.

## Command grouping

With the command line parameter `--debug-gpu` blender will add meta-data to the buffers and
commands. This makes it easier to navigate complex frame captures. Command grouping
is automatically enabled when running with the `--debug-gpu-renderdoc` option.

With the command `GPU_debug_group_begin(const char * name)` a new group can be added.
All groups must be closed by calling `GPU_debug_group_end()`. Many areas already have
these debug groups defined.

## OpenGL Unused Resource Renderdoc Bug

Most of the time, RenderDoc doesn't identify used resources (SSBOs, Images) and mark them as unused, hiding them from the shader resource lists.
This can be worked around by enabling the `Show Unusued Items` option.

## Limitations Renderdoc, Blender & Vulkan

There are some limitations when using Renderdoc to debug the Vulkan backend of Blender. 

### Dynamic rendering unused attachments

Renderdoc doesn't support `VK_EXT_dynamic_rendering_unused_attachments`. When running on
a platform that does support unused platforms the actual commands may differ. Unused attachments
will generate less pipelines.

### Wayland

Renderdoc has issues running wayland and vulkan. When using the Vulkan backend it is best
to disable wayland. `WITH_GHOST_WAYLAND=Off`
