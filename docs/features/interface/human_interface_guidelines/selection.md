# Selection

!!! Warning "WIP"

## Input Mapping

While the following mappings should be respected as much as possible,
it's expected that some parts of Blender may have to differ slightly.

!!! TODO
    Explain which mouse-button and modifier key should do what.

### 2D Editors

Selectable items in 2D editors (like lists, the Outliner tree, the files
in the File Browser, ...) should behave as follows:

- Right-mouse press selects (and activates) the item and shows the
  context menu.
  - If the item is already selected, don't deselect others.
  - If the item is not already selected, deselect others.

## Select-tweaking

Selecting and tweaking (dragging an item to move it) are overlapping
operations. Getting both to work without conflicts requires special care.

- Select-mouse press on an *un*selected item should immediately select
  it, and deselect all others.
  - If a tweak event is detected before a mouse release, the item should
    be translated (moved).
- Select-mouse press on a selected item should not change selection
  state (no undo push!). It should change the active item to be the
  clicked one where applicable.
  - If a tweak event is detected before a mouse release, **all selected
    items** should be translated (moved).
  - If a release event is detected, but no tweak event, all items but
    the clicked one should be deselected.

Implementing this behavior is a bit tricky. Utilitiy functions were
added to make things easier. As a reference for using these, check
blender/blender@7dea0585468d.

Note that for most editors, this behavior makes a tweak tool
unnecessary.

## Undo

Whenever scene data selection changes, an undo push should be triggered.
Conversely if selection did not change, no undo push should be
triggered. Neither should UI data selection changes cause undo pushes,
for example selection in the File Browser.
