# Animation System

This section describes Blender's current animation system, as well as the
currently in-development system.

## Animato

[Animato](animato/index.md) is the animation system introduced for Blender 2.50
in 2009. Many of its parts are still in use in Blender.

## Layered Actions

The "[Layered Actions](layered.md)" system is currently in development.
