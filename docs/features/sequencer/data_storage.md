# Data storage

There are 2 main data structures containing sequencer data: -
`Editing` holds data that belongs to sequencer core, this is mainly
strips. - `SpaceSeq` holds data that belongs to sequencer editor. This
defines editor state and configuration like render size, overlays and
various view settings.

`Editing` itself belongs to `Scene->ed`. Most important field is
`seqbase` which is linked list of all strips in timeline.
`metastack` defines list of meta strips being edited. Runtime data for
cache and prefetching are stored in `Editing` as well.

Sequencer also uses `SequencerToolSettings` which are stored in
`scene->toolsettings->sequencer_tool_settings`

## Strip data layout

This is quite confusing part of sequencer code - What is usually called
a strip is represented by data structure called `Sequence`. There is
also data structure with name `Strip` which defines data and some
properties used by `Sequence`. Many fields in `Sequence` structure
defines all properties of strip, mainly its position in timeline, type,
properties for image processing or sound, pointers to other datablocks,
effect data, etc.

Data structures nested in `Sequence`:

- `Strip`: As mentioned above, this defines mainly file paths for
  image and movie strips along with some properties. These will be
  described below.
- `StripProxy`: Defines proxies for image or movie strips. Proxy size,
  quality, storage methods and timecode(for movie strips only).
- `StripTransform`: Image transformation properties.
- `StripCrop`: Image crop properties.
- `StripElem`: Source image or movie filename. In case of image
  sequences, array of this structure is used. Some basic information
  about media is stored here, such as image resolution or movie FPS.
  This information may not be accurate and should be used carefully.

Offsets:

- `startdisp` / `enddisp` This indicates start and end frame of
  strip as it is truly presented in timeline. This is meant to be
  updated during editing. It is more of a runtime variable, but back
  then this wasn't differentiated by any means.
- `startstill` / `endstill` indicates how many frames are there in
  strip before/after actual image content. Strip can be longer than
  video for example - area with no image content should preview as still
  frame.
- `startofs` / `endofs` is offset from start of content. For
  example, to skip 200 frames from start, `startofs` should be set to
  `200`. If this happens there can be no still frames, but this is not
  prevented by design, it's prevented by implementation. This offset is
  also called split or cut in other words.
- `anim_startofs` / `anim_endofs` This is "hold split". If strip is
  hold split at 200 frames from start and start is extended, This will
  yield still frames.