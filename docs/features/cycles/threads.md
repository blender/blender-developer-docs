## Cycles Threads

### Types

We can distinguish between 3 threads:

- The **Blender Thread** is the one we are in when receiving render
  engine callbacks. From this thread it is safe to access the Blender
  scene. However, Blender is blocked while these callbacks are executed,
  so we must try to do as little work as possible here.
- The **Session Thread** sits between the threads that actually render
  the scene and the thread that updates data from Blender, doing scene
  updates and distributing tasks.
- **Device Threads** do the actual rendering on a scene representation
  optimized for rendering. For a CPU device we may have multiple such
  threads, for a GPU device

<img src="../../../images/Cycles_threads.png"/>

### What needs to improve

We currently have 3 scene representations, which is not great in terms
of memory usage. The question is how to get rid of this, while still
making it possible to synchronize efficiently.

Some ways to reduce the problem are to free data as soon as we no longer
need it. Especially for non-interactive rendering this is feasible.
Doing our own subdivision would also reduce the problem.

Blocking Blender while copying data out is also problematic for bigger
scenes. This could be tackled Blender side, running render engine
callbacks not from the main thread, though this requires mutex locks on
Blender scene data. We could also try to split up the synchronization
code on our side, so that we can synchronize partially and then give
back control to Blender, to continue synchronizing after Blender events
are handled.
