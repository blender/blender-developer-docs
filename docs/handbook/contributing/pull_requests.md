# Pull Requests

This page describes the tools used for code contribution and review. The
process for effective contributions is described in [Contributing
Code](index.md).

Code reviews are a key measure to assure changes are of a good quality.
They help preventing bugs, architectural issues or potential maintenance
problems. And having your code reviewed also generally keeps you on your
toes.

## One Time Setup

This assumes you have the Blender repository already checked out on your
computer, following the [build
instructions](../building_blender/index.md).

### Fork

- Go to [Blender repository](https://projects.blender.org/blender/blender) and click
  the **Fork** button.
  ![](../../images/Gitea_PR_CreateFork.jpg){width=600}

- Confirm the fork with the default settings.
- Now you will have to add your personal fork as a remote in your local
  git repository. Click **SSH** to see the correct URL, and then add it
  like this:

``` bash
git remote add me git@projects.blender.org:<USERNAME>/blender.git
git submodule sync
```

>? NOTE: **Alternative: GitHub Convention**
> It is also possible to use the common GitHub workflow, where `origin`
is your fork and `upstream` is `blender/blender`. For this do:
``` bash
git remote add upstream git@projects.blender.org:blender/blender.git
git remote set-url origin git@projects.blender.org:<USERNAME>/blender.git
make update
```
For the rest of this document, you will need to replace `origin` with
`upstream`, and `me` with `origin`.

### SSH Key

In order to push to the fork repository, you need an SSH key. If you
don't already have the file `~/.ssh/id_rsa.pub`, there's a simple
command to generate such keys which works on Linux, macOS, and in Git
Bash on Windows:

``` bash
ssh-keygen
```

This command will generate a private key `id_rsa` and a public key
`id_rsa.pub` in `~/.ssh`. The private key must never be shown or sent to
anyone else to avoid compromising your account, but the public key is
safe to share.

The contents of `~/.ssh/id_rsa.pub` can be copied and pasted into the
[account settings on
projects.blender.org](https://projects.blender.org/user/settings/keys),
after clicking "Add Key". Any name for the SSH key is ok.

### Test

Now you can test if your SSH key and remote URL works, there should not
be any errors when running this:

``` bash
git fetch me
```

### Email

To have the correct email associated with every commit, disable Hide
Email address in your profile settings. Otherwise for online editing and
some types of pull request merges, it may use the `@noreply.localhost`
domain.

The Blender repositories are mirrored to GitHub. If you like to
have commits associated with your account there, add the same email
address on your GitHub account. To have commits show up on your GitHub
profile page, star the [Blender repositories on GitHub](https://github.com/blender/).

## Workflow

### Create a Pull Request

See [Git resources](using_git.md#further-reading) to learn the
basics of Git, this guide assumes some familiarity.

In your git local git repository, make changes in a branch:

``` bash
git checkout -b my-feature main
git commit file1 file2
```

When you are done, push this branch to your fork:

``` bash
git push me my-feature
```

If all goes well, a message will be printed to the console with a link
to create a PR:

``` bash
remote: Create a new pull request for 'my-feature':
remote:   https://projects.blender.org/blender/blender/compare/main...username:my-feature
```

Alternatively, you can go to the page of your fork on
projects.blender.org. There you can select the branch, and click the
icon next to it create a PR from the branch.

Typically the pull request should be set to merge into the
`blender:main` branch.

![](../../images/Gitea_PR_CreatePR.jpg){width=600}

### Update a Pull Request

The code can be updated by simply pushing to the same branch, and the
changes will be reflected in the pull request.

Adding new commits and updating to the latest changes can be done as
follows. Reviewers are able to squash everything into a single commit
when needed.

``` bash
git checkout my-feature
git merge main            # Optionally merge with main to get the latest changes 
git commit file3 file4    # Add new commit
git push me my-feature
```

It is also possible to rewrite history with amend and rebase, which
requires a force push.

> WARNING:
> It is recommended to only do this before code review has started, as
> it is not as easy to see changes that have happened during the review
> process after a force push.

``` bash
git checkout my-feature
git rebase main                 # Optionally rebase on main to get the latest changes 
git commit --amend file3 file4  # Amend commit
git push -f me my-feature
```

### Handling Review Feedback
In almost all pull requests, reviewers will have some form of changes requested - this is
especially true for new contributors who are not as familiar with the codebase or common
practices.

#### Resolving Conversations
When individual lines or blocks of code have had changes requested, there are generally two
outcomes you will experience as a PR author.

1. If the requested change is something that you have addressed in a subsequent PR, marking the
conversation as resolved, with an optional comment, is helpful for both reviewers and other
readers to know the current state of the pull request.

2. If the requested change is something you disagree with, require further help on, or otherwise
have not made any concrete changes for, it is best to leave the conversation unresolved. This
signals that further discussion is expected to happen. After this, the reviewer may also resolve
the conversation if the discussion has been concluded.

#### Re-request Review
If a reviewer has requested changes, and the changes have been fixed in subsequent
commits on the pull request, the PR author should click on the "Re-request Review" button
to signal that the PR is ready for another look by the reviewer.

This is preferred to leaving a comment and/or mentioning reviewers by username, as it moves
the PR into a queue and avoids the chance of it getting lost in other notifications.

### Checkout a Pull Request

For a given pull request `#123`, try it out locally in a branch like
this:

    git fetch origin pull/123/head:new-feature-123
    git checkout new-feature-123

### Tests

Before merging, building and tests should pass on all platforms. [Run
tests locally](../testing/setup.md) to find issues before
submitting pull requests.

Reviewers can start tests on the
[buildbot](../tooling/buildbot.md) by including the
following line in a comment to the PR. See
[blender-bot](https://projects.blender.org/infrastructure/blender-bot/src/branch/main/README.md)
for details.

    @blender-bot build

![](../../images/Pull_request_checks.png){width=600}

### Merge a Pull Request

First check carefully that the commits and files changes are as
expected, and that the branch you are merging into is correct. Be
careful not to merge a branch based on main into a release branch.

Reviewers can merge pull requests directly from projects.blender.org.
There are three strategies:

- **Squash**: squash everything into one commit, using the PR title and
  description as the default commit message. You have the opportunity to
  edit the commit message before the push goes through.
- **Rebase and fast-forward**: push all the commits in the PR, with
  commit messages unchanged except `Pull Request #123` being added at
  the end of the last commit.
- **Manually merged**: we recommend landing through the web UI when
  possible. However if a pull request has been manually pushed this
  option can be used to close the PR and link it to the appropriate
  commit. The full (long) commit hash must be provided. Always mention
  the pull request \# in the commit message.

Make sure the commit message follows the
[guidelines](../guidelines/commit_messages.md). For squashing,
convert the commit message to plain text and remove images, as Gitea
does not render markdown in commit messages.

### Rebasing a Pull Request

Sometimes a PR needs to go into a different target branch. For example,
if a PR started to be worked on `main` but then needs to be included
in a release branch `blender-vX.Y-release`. You then rebase the PR
instead of creating a new one.

Go to the pull request page, click `Edit` next to the title, choose a
different target branch, and press `Save`.

The number the commits in the PR will have changed. Now you **must
rebase your local branch** onto the new target branch, to make sure
commits that do not belong in the PR are removed. Here are two way of
doing it:

>? NOTE: **Rebase Onto**
> We will show how to change from `main` to `blender-vX.Y-release`.
>
> Make sure you have all the latest upstream changes
> 
> ``` bash
> git fetch origin
> ```
> 
> Checkout the feature branch and rebase onto the target branch.
> 
> ``` bash
> git checkout my-feature
> git rebase --onto origin/blender-vX.Y-release origin/main my-feature
> ```
> 
> There might be conflicts, so you will have to resolve these manually.
> 
> Now carefully check the output of `git log` to see your branch
> contains only the required changes. Test that the code still builds and
> works correctly.
> 
> Finally you can push the changes. Since you have rewritten the history,
> you need to force push the changes.
> 
> ``` bash
> git push -f me my-feature
> ```


>? NOTE: **Interactive Rebase**
> We will show how to change from `main` to `blender-vX.Y-release`.
> 
> Make sure the target branch is up-to-date.
> 
> ``` bash
> git checkout blender-vX.Y-release
> git pull --rebase origin
> ```
> 
> Then start the rebase:
> 
> ``` bash
> git checkout my-feature
> git rebase -i blender-vX.Y-release
> ```
> 
> This will now show you a list of commits (it might be very long) in your
> configured editor. Make sure to only pick the commits that you want in
> your PR. Delete the lines with unrelated commits.
> 
> There might be conflicts, so you will have to resolve these manually.
> 
> Now carefully check the output of `git log` to see your branch
> contains only the required changes. Test that the code still builds and
> works correctly.
> 
> Finally you can push the changes. Since you have rewritten the history,
> you need to force push the changes.
> 
> ``` bash
> git push -f me my-feature
> ```

### Update a Contributor's Pull Request

Sometimes a pull requests needs a simple change before merging, like
fixing formatting or rebasing to the right branch. In this case it's
possible for Blender developers to update the pull request branch in the
fork, if "Allow Edits by Maintainers" is enabled as it is by default.

The basic syntax is the following:

``` bash
git push git@projects.blender.org:PR_AUTHOR/blender.git LOCAL_BRANCH:PR_BRANCH
```

## Quality Checklist

There are a number of common mistakes in contributions. This checklist
can help both contributors and reviewer help verify everything is taken
into account.

- Is backward compatibility with existing .blend files and the Python
  API preserved?
- Is the naming and user interface consistent with related functionality
  in Blender?
- Can the feature be made easier or more efficient to use?
- Does the change have a negative impact on performance?
- Can a little refactoring help make the code easier to understand and
  maintain?
- Does the code have comments?
- Was [clang-format](../tooling/clangformat.md) used to format the
  code to follow the conventions?
- Do the [automated tests](../testing/setup.md) still pass?

## Guidelines for Reviewers

- The pull request text should be usable as the git commit message (see
  the [guidelines](../guidelines/commit_messages.md) for details).
- Be explicit when some changes are to be addressed before committing,
  without the need for a review iteration.
- If the pull request is not approved the author is expected to make
  another iteration.
- If the change needs agreement on the design task first, put the pull
  request on hold by adding a "WIP: " prefix in the title, indicating
  the author considers the pull request not ready to be merged. No
  review is expected unless the author specifically asks for it.
- Developers are expected to reply to pull requests in 3 working days.
- Add relevant modules/projects to tags.
- Assign individuals (instead of modules/projects) for reviewers, to
  avoid too much noise.
- Encourage new developers to do code review, it's a good way to learn
  and important to grow the project.

## Tips

- To get the patch file, add `.patch` to the end of the URL of the
  pull request. Example:

<!-- -->

    https://projects.blender.org/blender/blender/pulls/104892.patch

- Checkout a pull request into a detached head (not leaving behind a
  branch). Example:

``` bash
git fetch -q origin +refs/pull/104892/head: ; git checkout -qf FETCH_HEAD
```
