# Building Cycles with GPU Binaries

For GPU accelerated rendering with Cycles on various graphics cards,
additional build configuration is needed.

## NVIDIA

### CUDA

Install [CUDA Toolkit 11 or 12](https://developer.nvidia.com/cuda-downloads). Newer versions may
work but are not tested as well, older versions will not work.

Change the [CMake
configuration](../building_blender/index.md#customization) to enable
building CUDA binaries:

``` text
WITH_CYCLES_CUDA_BINARIES=ON
```

If you will be using the build only on your own computer, you can
compile just the kernel needed for your graphics card, to speed up
building. For that you need to edit `CYCLES_CUDA_BINARIES_ARCH` in the
CMake configuration, and leave only the [architecture needed for your
GPU](https://developer.nvidia.com/cuda-gpus).

### OptiX

For rendering with OptiX to take advantage of hardware acceleration in
RTX cards, you need to install the [OptiX SDK
7.4](https://developer.nvidia.com/designworks/optix/download). This is
supported on Windows and Linux currently.

CUDA must first be set up as described above.

Change the [CMake
configuration](../building_blender/index.md#customization) to enable
building with OptiX and point to the OptiX installation directory:

``` text
WITH_CYCLES_DEVICE_OPTIX=ON
OPTIX_ROOT_DIR=/path/to/NVIDIA-OptiX-SDK-7.x.x-xxx
```

### Incompatible Compilers

When using relatively new GCC versions, the CUDA toolkit may not yet be
compatible with them.

For example CUDA toolkit 12 was not initially compatible with GCC 13.
To solve this an older GCC version can be used.

In this example GCC 12 would work, and `make` can be run with the following
arguments to specify a different compiler version:

``` bash
make CC=gcc-12 CPP=g++-12 CXX=g++-12 LD=g++-12
```

### Debug Builds

[ASAN](../tooling/asan.md) interferes
with CUDA availability. Disable it to avoid CUDA errors on
initialization.

``` text
WITH_COMPILER_ASAN=OFF
```

## AMD

### Linux

Install ROCm 5.x with the HIP compiler, use the most recent version
to get important bug fixes.
[Instructions by AMD](https://rocm.docs.amd.com/en/latest/deploy/linux/quick_start.html).
Some Linux distributions may also provide this as packages. Installing
drivers is not required for compiling, only for running.

Change the CMake configuration to enable building CUDA binaries:

``` text
WITH_CYCLES_HIP_BINARIES=ON
```

If you will be using the build only on your own computer, you can
compile just the kernel needed for your graphics card, to speed up
building. For that you need to edit `CYCLES_HIP_BINARIES_ARCH` in the
CMake configuration, and leave only the [needed for your
GPU](https://llvm.org/docs/AMDGPUUsage.html#processors) (this list is
incomplete unfortunately, so it may not be trivial to find your GPU).

#### Rocky Linux

The following commands are used on the buildbot machines. Kernel drivers are left out, since these machines do not have AMD GPUs.

``` bash
sudo sh -c 'echo "[ROCm]
name=ROCm
baseurl=https://repo.radeon.com/rocm/yum/6.1.3/main
enabled=1
gpgcheck=1
exclude=rock-dkms
gpgkey=https://repo.radeon.com/rocm/rocm.gpg.key" > /etc/yum.repos.d/rocm.repo'

sudo rpm --import https://repo.radeon.com/rocm/rocm.gpg.key

sudo yum install -y hipcc hip-devel rocm-llvm rocm-core rocm-device-libs6.1.3.x86_64
```

### Windows

Install the [HIP SDK](https://www.amd.com/en/developer/resources/rocm-hub/hip-sdk.html) for
windows. By default the installer creates the environment variable
`HIP_PATH` which is used in cmake for locating the SDK location.

To enable building HIP kernels set:

``` text
WITH_CYCLES_HIP_BINARIES=ON
```

To enable building HIPRT kernels (hardware accelerated ray tracing) set:

``` text
WITH_CYCLES_DEVICE_HIPRT=ON
```

## Intel

OneAPI support is available on Windows & Linux, for Intel Arc GPUs.
Building must be done using precompiled libraries, which includes the
compiler for OneAPI (there is currently no Intel provided SDK that works
with Blender).

Enable it in the build configuration as follows:

``` text
WITH_CYCLES_DEVICE_ONEAPI=ON
```

### Ahead-of-Time Compilation

It is possible to compile GPU kernels in an ahead-of-time manner as a
step of Blender build process. In order to do so the following options
should be enabled:

``` text
WITH_CYCLES_ONEAPI_BINARIES=ON
```

On Windows it is also required to download [Offline Compiler for
OpenCL](https://www.intel.com/content/www/us/en/developer/articles/tool/oneapi-standalone-components.html).
The path to it is either to be provided using `OCLOC_INSTALL_DIR`
CMake variable, or the `ocloc` is to be placed to
`win64_vc15/dpcpp/lib/ocloc`.

## Apple

GPU rendering with Metal on Apple requires no extra build steps, the
kernels are compiled at runtime.
