---
hide:
  - toc
---

# Hardware List

A list of hardware in use by active Blender developers.

This helps to find people with certain hardware, to confirm bug reports or help out testing new code. 


| **Name** | **CPU** | **GPU** | **RAM** | **OS** | **Additional hardware (e.g. tablet/pen)** |
| -- | -- | -- | -- | -- | -- |
| [Aaron Carlisle](https://projects.blender.org/Blendify) | Intel Core i5-8250U (4-Core) | Intel HD Graphics 620 | 12GB | Ubuntu 20.10 | |
| [Aaron Carlisle](https://projects.blender.org/Blendify) | AMD Ryzen 7 3700X (8-Core) |	NVIDIA GeForce GTX 1060 & NVIDIA GeForce RTX 2070 | 32GB | Ubuntu 20.10 / Windows 11 | |
| [Alaska](https://projects.blender.org/Alaska) | AMD Ryzen 9 5950X (16-core) | NVIDIA GeForce RTX 4090 | 64GB | Windows 11 | PSVR 2 with PC adaptor |
| [Alaska](https://projects.blender.org/Alaska) | AMD Ryzen 9 3900X (12-core) | AMD RX 7800XT/Intel Arc A750 | 32GB | Windows 11/Linux | |
| [Alaska](https://projects.blender.org/Alaska) | Apple M4 Pro (12-core) | Apple M4 Pro | 48GB | macOS 15 | XDR/HDR display, Wacom Intuos CTL-6100 |
| [Alaska](https://projects.blender.org/Alaska) | Intel Core i7 6700HQ (4-core) | Intel HD Graphics 530/NVIDIA GTX 960M | 16GB | Window 10 | |
| [Alaska](https://projects.blender.org/Alaska) | Intel Core i7 4702MQ (4-core) | Intel HD Graphics 4600/NVIDIA GT 750M | 16GB | Window 10 | |
| [Antonio Vazquez](https://projects.blender.org/antoniov) | Intel Core i9-9900K (8-Core) | NVIDIA GeForce RTX 2080TI | 32GB | Windows 10 | Wacom Intuos 3 |
| [Bastien Montagne](https://projects.blender.org/mont29) | AMD Ryzen 7 4800HS (8-Core) | AMD Renoir integrated | 48GB | Debian testing | |
| [Brecht Van Lommel](https://projects.blender.org/brecht) | AMD Ryzen 2990wx (32-Core) | NVIDIA Quadro RTX A6000 | 48GB | Ubuntu 20.04 | |
| [Brecht Van Lommel](https://projects.blender.org/brecht) | Apple M1 | Apple M1 | 16GB | macOS 11 | |
| [Campbell Barton](https://projects.blender.org/ideasman42) | AMD Ryzen Threadripper 3970X (32-Core) | AMD Radeon RX 5600 | 64GB | Arch Linux | Wacom Intuos 3, 3Dconnexion Space Mouse Pro |
| [Clément Foucault](https://projects.blender.org/fclem) | AMD Ryzen Threadripper 3970X (32-Core) | AMD Radeon RX 5600 | 64GB | Arch Linux | |
| [Clément Foucault](https://projects.blender.org/fclem) | Apple MacBook Pro (Retina, 13-inch, Early 2015) | Intel Iris Graphics 6100 | 1.5GB | macOS 11 | |
| [Dalai Felinto](https://projects.blender.org/dfelinto) | - | NVIDIA GeForce GTX 1080 | - | Ubuntu | |
| [Germano Cavalcante](https://projects.blender.org/mano-wii) | AMD Ryzen 9 7950x | AMD Radeon RX 480 | 24GB | Windows 11 | |
| [Germano Cavalcante](https://projects.blender.org/mano-wii) | Apple M1 | Apple-designed integrated graphics | 8GB | macOS Big Sur (11.5.2) - ARM 64 Bits | |
| [Germano Cavalcante](https://projects.blender.org/mano-wii) | AMD Ryzen 7 5800H | NVIDIA GeForce RTX 3060 Laptop GPU/PCIe/SSE2 | 16GB | Windows 11 | |
| [Hans Goudey](https://projects.blender.org/hooglyboogly) | AMD Ryzen 9 7950X | Ryzen 6700XT | 64GB | Fedora 38 | |
| [Harley Acheson](https://projects.blender.org/Harley)  | Intel Core i9-13900K (24-Core) | NVIDIA GeForce RTX 4080 | 32GB | Windows 11 | |
| [Himanshi Kalra](https://projects.blender.org/Calra) | Intel Core I5-8250U (4-Core) | Intel UHD Graphics 620 && NVIDIA GeFornce MX150 | 8GB | Windows 11 | Wacom One |
| [Jacques Lucke](https://projects.blender.org/JacquesLucke) | AMD Ryzen 9 3900X (12-Core) | AMD Radeon RX 5700 | 48GB | Fedora | |
| [Jeroen Bakker](https://projects.blender.org/Jeroen-Bakker) | Intel Core i7-6700 (4-Core) |  | 64GB | Linux Ubuntu 24.04 | |
| [Jeroen Bakker](https://projects.blender.org/Jeroen-Bakker) | AMD Ryzen 5800 (8-Core) | AMD Radeon PRO W7900 | 64GB | Linux pop-OS/Windows 10 | |
| [Jeroen Bakker](https://projects.blender.org/Jeroen-Bakker) | AMD Ryzen 9 7950X (32-Core) | AMD Radeon Graphics (RADV RAPHAEL_MENDOCINO), Intel ARC 750 Graphics (DG2), NVIDIA Quadro RTX 6000 | 32GB | Linux pop-OS | |
| [Jeroen Bakker](https://projects.blender.org/Jeroen-Bakker) | AMD Ryzen 1700 (8-Core) | AMD Radeon PRO 6600 (ES) | 24GB | Windows | |
| [Jeroen Bakker](https://projects.blender.org/Jeroen-Bakker) | Intel Core i7-1065G7 (4-Core) | iGPU | 16GB | Windows 10 | |
| [Jeroen Bakker](https://projects.blender.org/Jeroen-Bakker) | Mac Studio M1 - Ultra | M1 GPU | 128GB | macOS 14.6.1 | |
| [Jeroen Bakker](https://projects.blender.org/Jeroen-Bakker) | Mac Mini Intel | Intel iGPU UHD630 | 16GB | macOS 14.6.1 | |
| [Jesse Yurkovich](https://projects.blender.org/deadpin) | Intel Core i7-13700K (24-Core) | NVIDIA GeForce RTX 4090 | 128GB | Windows 11 / Windows 10 / WSL2 Ubuntu 22.04 | |
| [Julian Eisel](https://projects.blender.org/JulianEisel) | Intel Core i9-9900K (8-Core) | AMD Radeon Vega Frontier Edition | 32GB | Linux pop-OS | |
| [Julian Eisel](https://projects.blender.org/JulianEisel) | Intel Core i9-10910 (10-Core) | AMD Radeon Pro 5700 XT | 64GB | macOS Monterey (12.3) | Retina Display |
| [Julian Eisel](https://projects.blender.org/JulianEisel) | Intel Core i9-9880H (8-Core) | AMD Radeon Pro 5500M | 16GB | macOS Monterey / Windows 10 | Retina Display |
| [Kévin Dietrich](https://projects.blender.org/kevindietrich) | AMD Ryzen 9 5900X (12-Core) | NVIDIA GeForce RTX 2070 | 32GB | Linux Mint 20 | Wacom Bamboo CTH-461 |
| [Omar Emara](https://projects.blender.org/OmarEmaraDev) | AMD Ryzen 7 1700 (8-Core) | AMD HD5450/AMD RX590 | 8GB | Arch Linux | Wacom Intuos CTH480 |
| [Peter Kim](https://projects.blender.org/muxed-reality) | Intel Core i7-7700HQ (4-Core) | NVIDIA GTX 1060m / AMD Radeon Vega 64 | 32GB | Windows 10 | Lenovo Explorer WMR Headset, Oculus Quest 2 |
| [Philipp Oeser](https://projects.blender.org/lichtwerk) | AMD Ryzen™ 9 5900HX (8-Core) | NVIDIA GTX 3080m | 32GB | Linux Fedora 39 | Huion HS610 |
| [Philipp Oeser](https://projects.blender.org/lichtwerk) | Apple M2 | Apple M2 | 24GB | Sonoma 14.4.1 | |
| [Pratik Borhade](https://projects.blender.org/PratikPB2123) | Intel Core i5-8250U (4-Core) | AMD Radeon 535 | 8GB | Windows 10 | |
| [Pratik Borhade](https://projects.blender.org/PratikPB2123) | AMD Ryzen 7 5800H (8-Core) | NVIDIA GeForce RTX 3050 | 16GB | Windows 11 | |
| [Pratik Borhade](https://projects.blender.org/PratikPB2123) | Apple M3 Pro (12-core) | Apple M3 Pro | 36GB | Sonoma 14.1 | |
| [Robert Gützkow](https://projects.blender.org/rjg) | Intel Core i7-6850K | NVIDIA GeForce GTX 1080 Ti | 32GB | Windows 10 | Wacom Intuos CTH-490 |
| [Robert Gützkow](https://projects.blender.org/rjg) | Intel Core i7-4800MQ | Intel® HD-Graphic 4600 | 8GB | Linux (Ubuntu) | |
| [Richard Antalik](https://projects.blender.org/iss) | AMD Ryzen 9 3950X (16-Core) | AMD Radeon RX 550 | 48GB | Xubuntu | |
| [Richard Antalik](https://projects.blender.org/iss) | Intel Core i9 (10-Core) | AMD Radeon Pro 5700 XT 16GB | 64GB | macOS 12.6 | |
| [Richard Antalik](https://projects.blender.org/iss) | AMD Ryzen 7 4700U (8-Core) | Integrated | 16GB | Windows 10 | |
| [Sergey Sharybin](https://projects.blender.org/Sergey) | Apple M2 Ultra | M2 GPU | 192GB | macOS (usually up-to-date) | |
| [Thomas Dinges](https://projects.blender.org/ThomasDinges) | AMD Ryzen 9 5950X (16-Core) | AMD Radeon RX 5500 XT | 32GB | Windows 10 | |
| [Thomas Dinges](https://projects.blender.org/ThomasDinges) | Intel Core i5-6200U (2-Core) | Intel HD Graphics 520 | 8GB | Windows 10 | |
| [Wu Yiming](https://projects.blender.org/ChengduLittleA) | Intel Core i9-13900K (32-Threads) | NVIDIA RTX 4070 12GB | 64GB | Ubuntu 22.04 / Windows 11 | Wacom Cintiq Pro 24 / Pro Pen / Art Pen / Saitek x56 |
| [Wu Yiming](https://projects.blender.org/ChengduLittleA) | Intel Core i7-4910MQ (8-Threads) | NVIDIA Quadro K5100M 8GB | 32GB | Ubuntu 22.04 | same pen deivce as above |
| [Wu Yiming](https://projects.blender.org/ChengduLittleA) | Intel Core i7-8650U (8-Threads) | Intel UHD Graphics 620 | 16GB | Ubuntu 22.04 / Windows 11 / WSL2 | Surface Pro Pen 2 / Slim Pen |

And here is a list of hardware in use by active volunteers.

| **Name** | **CPU** | **GPU** | **RAM** | **OS** | **Additional hardware (e.g. tablet/pen)** |
| -- | -- | -- | -- | -- | -- |
| [Ankit Meel](https://projects.blender.org/ankitm) | Intel Core i5-5350U (2-Core) | Intel HD Graphics 6000 | 8GB | macOS Monterey (12.7) | |
| [Ankit Meel](https://projects.blender.org/ankitm) | Apple M3 Pro 12 Core | Apple M3 Pro | 18 GB | macOS Sonoma (14.6.1) | |
| [Aras Pranckevicius](https://projects.blender.org/aras_p) | Apple M4 Max | Apple M4 | 48GB | macOS Sequoia (15.1) | |
| [Aras Pranckevicius](https://projects.blender.org/aras_p) | AMD Ryzen 5950X (16-Core) | NVIDIA GeForce RTX 3080 | 32GB | Windows 10 | |
| [Jonas Holzman](https://projects.blender.org/Brainzman) | Apple M1 Max | Apple M1 Max | 64GB | macOS Sequoia 15.0 |  Retina/HDR display, Wacom Intuos CTL-6100WL |
| [Howard Trickey](https://projects.blender.org/howardt) | Apple M3 Max (16-Core) | Apple M3 Max | 64GB | macOS Sonoma 14.5 | |

List of GPUs at Blender HQ available for bug fixing.

> **NOTE**: Check with [Jeroen Bakker](https://projects.blender.org/Jeroen-Bakker).

| **GPU** | **Architecture** |
| ------- | ---------------- |
| AMD Radeon 890M| RDNA3.5 |
| AMD PRO Radeon W7900 | RDNA3 |
| AMD PRO Radeon W7800 | RDNA3 |
| AMD PRO Radeon W7700 | RDNA3 |
| AMD PRO Radeon W7600 | RDNA3 |
| AMD PRO Radeon W7500 | RDNA3 |
| AMD Radeon RX 7900 XT | RDNA3 |
| AMD PRO Radeon W6800 | RDNA2 |
| AMD PRO Radeon W6600 | RDNA2 |
| AMD Radeon RX 5700 XT | RDNA1 |
| AMD Radeon RX 5700 | RDNA1 |
| AMD Radeon Vega 64 | Vega |
| AMD PRO Radeon WX9100 | Vega |
| AMD Radeon RX 480 | Polaris |
| AMD PRO Radeon WX7100 | Ellesmere |
| AMD PRO Radeon WX7100 DUO | |
| NVIDIA GTX 960 | |
| NVIDIA GTX 760 | |
| NVIDIA GTX 710 | |
| NVIDIA GTX 1060 | Pascal |
| NVIDIA GTX 1080 | Pascal |
| NVIDIA GTX 1080 ti | Pascal |
| NVIDIA Quadro FX 4800 | |
| NVIDIA Quadro RTX 5000 | Turing |
| NVIDIA Quadro GP100 | Pascal |
| NVIDIA Titan V | Volta |
| NVIDIA RTX A2000 | Ampere |
| Intel ARC A750 | Alchemist |
| Intel ARC A770 | Alchemist |
