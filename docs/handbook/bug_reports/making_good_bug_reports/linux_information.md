# Linux System Information 

## Blender 4.3+ - Semi-Automated Information Collection

In Blender 4.3 we added a script that helps automatically collect system information
in the case that Blender isn't opening. The steps to use it are as follows:

- Navigate to where Blender is installed.
  - If you installed Blender through Steam, then you can find where Blender is
  installed by right clicking on Blender in Steam selecting
  `Manage > Browse local files`.
  - If you installed Blender through the Snap store, then you can usually
  find Blender at `/snap/blender/current/`
- Inside the folder where Blender is installed you should find
`blender-system-info.sh`.
- Open a Terminal app and run this script.
- The script will collect information about your computer and Blender
and open the bug reporting web page, automatically filling it out with
the information it collected.
- The script is unable to collect information about your graphics card, so you will
need to collect this information manually by following `Graphics Card` section of
the guide below.

---

## Operating System

We need to collect a few pieces of information for the Operating System information
field. They are the Linux kernel version, CPU architecture, GLIBC version,
and display server type.

Follow the steps below to collect the information and combine them together
in the bug report form like this:
`Linux-6.8.0-31-generic - x86_64 - glibc2.39 - wayland`

#### Linux Kernel Version

Run `uname -r` in a terminal.

![](./images/linux_get_os_info_kernel.jpg)

#### CPU Architecture

Run `lscpu | grep Architecture` in a terminal.

![](./images/linux_get_os_info_architecture.jpg)

#### GLIBC

Run `ldd --version` in a terminal and extract the GLIBC version from the
first line of the output.

![](./images/linux_get_os_info_glibc.jpg)

#### Display Server

Run `echo $XDG_SESSION_TYPE` in a terminal.

![](./images/linux_get_os_info_display_server.jpg)

## Graphics Card

There are two main ways we recommend to collect GPU information on Linux. 
`glxinfo`, or `lspci` if `glxinfo` isn't available.

[comment]: <> (Use `vulkaninfo` instead of `glxinfo` once Blender uses Vulkan)

#### glxinfo

Run `glxinfo -B` in a terminal (you may need to install `glxinfo` from your
package manager). This will provide you with the GPU name, driver type, and
driver version of the display GPU (the GPU usually used by Blender).
Combine these details together and include them on your bug report form like this:
`AMD Radeon RX 7800XT - Mesa 24.1.0-devel`

![](./images/linux_get_gpu_info_glxinfo.jpg)

#### lspci

Run `lspci -k | grep -EA3 'VGA|3D|Display` in a terminal.
This will tell you which graphics cards you have. Copy the GPU name
and include it on your bug report form like this: `AMD Radeon RX 7700 XT / 7800 XT`

![](./images/linux_get_gpu_info_lspci.jpg)

## Blender Version

Follow one of the guides below to obtain Blender version information,
The information we're interested in are Blender version, commit date, hash,
and branch *(Blender 4.2+)*. Collect this information and share it on your
bug report form like this:
`Blender 4.2.0, branch: main, commit date: 2024-05-16 20:40, hash: 1558bc7cb428`

### Downloaded from Blender Website

Run `/path/to/blender --verison` in a terminal. The Blender version information
will be visible in the output.

![](./images/linux_get_blender_version.jpg)

### Downloaded from Steam

Open Steam, right click on Blender and select the option
`Manage > Browse local files`. A file browser will open displaying the path
to where Blender is installed. Open a terminal and run
`/path/to/blender --verison`. The Blender version information
will be visible in the output.

![](./images/linux_get_blender_version_steam.jpg)

### Downloaded from snap

Open a terminal and run `blender --version`. The Blender version information
will be visible in the output.

![](./images/linux_get_blender_version_snap.jpg)
