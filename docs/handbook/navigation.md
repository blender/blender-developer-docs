- [New Developers](new_developers/)
- [Communication](communication/)
- [Building Blender](building_blender/)
- [Development Environments](development_environments/)
- [Contributing Code](contributing/)
- [Organization](organization/)
- [Guidelines](guidelines/)
- [Release Process](release_process/)
- [Automated Tests](testing/)
- [Tooling](tooling/)
- [Design](design/)
- Bug Reports
  - bug_reports/*
- [Extensions](extensions/)
- [Translating](translating/)
