# Examples

* [Assets](assets.md)

---

## Additional Design Applications

Answering some [fundamental questions](../decision_tree.md) to better frame the problem:

* [Camera Lens](camera_lens.md)

Systematic view of a project, and how design is intertwined:

* [Geometry Nodes](geometry_nodes.md)