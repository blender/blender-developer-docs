# Icons

Icons UI Toolkit on [Penpot](https://www.penpot.app/).

![](../../../images/Design_UI_toolkit_icons_cover.jpg){style="width:800px;"}

## Download / Install

* [Penpot Blender Icon Set](https://download.blender.org/design/BlenderIconsSet_latest.penpot).

Install the `.penpot` in your penpot installation.
Add the file as a Library into your design documents.

![](../../../images/Design_UI_toolkit_icons_sample.jpg){style="width:800px;"}

## Generating the Icons

The icons were built with two separate parts:

 * A [script](https://projects.blender.org/dfelinto/penpot-icon-generator) that creates a SVG
with all the individual icons SVGs combined, to be pasted in penpot.
 * A [Penpot plugin](https://projects.blender.org/dfelinto/penpot-icon-generator-plugin)
that takes the pasted SVG and converts into individual components.

### Requirements

The scripts require SVGs to have all their elements to be inside a top group.
The icons categories and names are extracted from `UI_icons.hh`.
