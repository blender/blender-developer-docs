# Vim

## EditorConfig

Blender comes with a `.editorconfig` file that specifies conventions like
indentation for C/C++, Python, CMake and other code.

The [editorconfig-vim plugin](https://github.com/editorconfig/editorconfig-vim)
can be used to let Vim take it into account.

## ClangFormat

Multiple plugins are available, as well as a
[script bundled with clang-format](https://clang.llvm.org/docs/ClangFormat.html#vim-integration).

The bundled script can be configured like this to format on file save:

``` vim
function! Formatonsave()
  let l:formatdiff = 1
  py3f <insert path to>/clang-format.py
endfunction
autocmd BufWritePre *.h,*.c,*.cc,*.cpp,*.m,*.mm,*.glsl,*.osl call Formatonsave()
```

## RST

For more conveniently editing reStructuredText for the manual, add the following to your `.vimrc`.

To avoid conflict with any of your preferred settings for other file-types:
`"autocmd FileType"` is used where possible.

``` vim
" Example '.vimrc'

" needed to detect '.rst' files for special handling.
filetype plugin on

" indent based on filetype
filetype indent on

" These settings will only impact editing '.rst' files.
" so as not to conflict with any of your other settings.

" indentation settings
autocmd FileType rst setlocal expandtab shiftwidth=3 tabstop=3

" show a ruler at the right hand margin
autocmd FileType rst setlocal colorcolumn=120

" for wrapping text
autocmd FileType rst setlocal textwidth=118

" set spelling
autocmd FileType rst setlocal spell spelllang=en_us

" enable syntax highlighting
autocmd FileType rst syntax on

" all files use utf-8 encoding
autocmd FileType rst setlocal encoding=utf-8
```

### InstantRST (optional)

!!! Note
    This has not been tested in a while and information may be outdated.

This is a short guide on how I've been editing the blender-manual with
real-time feedback (WYSIWYG).

This guide relies on vim and the **Instant-rst** system by
[Rykka](https://github.com/Rykka)

#### Installation

1. [Build the Blender user manual](https://docs.blender.org/manual/en/latest/contribute/index.html)
   (for this guide I'm assuming the sources are in **~/blender-manual**)

2. Install [InstantRst](https://github.com/Rykka/InstantRst_link)
   (this is a plugin for vim)

3. Install
   [InstantRst Server](https://github.com/dfelinto/instant-rst.py/tree/blender-manual-static-templates_link)
   (this is a patched python plugin)

#### Running the Server

``` bash
instantRst -f ~/blender-manual/manual/render/workflows/bake.rst -i ~/blender-manual/manual/images
```

#### Editing the Manual

``` bash
vim ~/blender-manual/manual/render/workflows/bake.rst
```

This will open the file in vim, and now you can run the plugin

``` bash
:InstantRst
```

Start editing and look at your browser being updated while you edit it.

#### Further Considerations

Some RST syntaxes are not supported by Instant-Rst server
(in particularly `guilabel`, which we use a lot).
So it's good to build the docs as you would normally to make sure everything looks fine.
In this case you could either do **make** to build everything,
or **make render** to build only the parts relative to the rendering.
