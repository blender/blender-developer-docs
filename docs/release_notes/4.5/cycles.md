# Blender 4.5 LTS: Cycles


## Adaptive Subdivision

* Support previously missing features:
  * Attribute subdivision
  * Smooth UV subdivision
  * Motion blur
* Reduced vertex and triangle count when no subdivision is needed.