# Blender 4.5 LTS: Modeling & UV

### Curves

Many operators have been implemented for the new curves type

* "Separate" and "Join" operators (blender/blender@a99f9496a082, blender/blender@5a6d2e1ce29b).
* "Convert Attribute" operator (blender/blender@51f719cfab93).
* "Split" operator (blender/blender@2c4229455747).