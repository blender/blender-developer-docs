# Blender 4.5 LTS: User Interface

## General

- Improve "Saved" report message to better differentiate between regular saving, "Save As", "Save Incremental", or "Save Copy". (blender/blender@43b3c62a54)
- Tooltips now show disabled message above python path. (blender/blender@24fe1f2d5c)
- Tooltips for assets now include the file name and directory path of the asset. (blender/blender@5ba668135a)
- Themeable RedAlert, Error, Warning, Info Colors. (blender/blender@5956752eb7)
- Reduced size of warning icons on large dialogs. (blender/blender@497f2884e8)
- Improved "Presets" icon. (blender/blender@0381f18429)
- Improved scrollbars with handles. (blender/blender!135023)

## 3D Viewport

- Show object data name in 3D viewport text overlays. (blender/blender@55de54557c)
- Shading popover: Move Cavity properties into separate subpanel. (blender/blender@9627bdfab3)

## Asset Browser

- _Generate Preview_, _Generate Preview from Object_ and _Remove Preview_ operate on all selected assets instead of only the active one. (blender/blender@32cae542da27)

## Status Bar
- Tightened spacing. (blender/blender@beaaa428e9)
- Improved display during text and numerical entry. (blender/blender@16a4956ce8)

## Platforms

- macOS: Corrected handling of inverted Y tilt axis.  (blender/blender@c551d2a819)

## Node Editor

- Panels in node groups can now have toggle inputs. (blender/blender@2822777f13).