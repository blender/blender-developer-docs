# Blender 4.5 LTS: EEVEE & Viewport


# Navigation & Selection

Objects that are not marked selectable no longer affect selection and auto-depth.
This improves working with semi transparent surfaces. (blender/blender@3dfec1ff7387239da75d1ca3fe675a192469fdfb)

# GPU based subdivision

GPU based subdivision is available using on Metal (blender/blender@32999913efb66e59dfe306f38df3f898a65a7d29)
and Vulkan (blender/blender@c7f7c03f4cc10d25728b8c0550b59d82c02bbb57).

Some performance figures: Suzanne 6 subdivision levels

| Machine         | CPU Subdivision | GPU Subdivision |
| --------------- | --------------- | --------------- |
| M1 Studio Ultra | 7fps            | 12 fps          |
| M2 Air          | 3fps            | 11 fps          |

> [!NOTE] Add a graph and performance data for Vulkan