# Blender 4.5 LTS: Compositor

## Added

- The Image node now support vector outputs from EXR images. Those were previously exposed as color
  outputs. (blender/blender@370748cbb6)

## Changed

- Vector images now consume less memory. (blender/blender@5e8f96277d)