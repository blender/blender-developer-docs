# Blender 4.5 LTS: Python API

## Additions

### User Interface

- `KeyConfig.keymaps.find_match(keymap)`

  Add a convenience function to find the matching key-map from another key-configuration.

  This avoids having to pass multiple arguments to the existing `find` method.

  (blender/blender@28126b83a527c081677d45906e2d18dc1c0bf976).


- `KeyMap.keymap_items.find_match(keymap, keymap_item)`

  Add a method to support looking up key-map items from other key-maps.

  This allows an add-ons preferences to show the key-map editor for key-map items
  they define while maintaining the original key-map entries (used by "Reset to Default").

  Typically the `find_match` method should be called on key-maps from:
  `context.window_manager.keyconfigs.user`
  to lookup key-map items which the add-on created in:
  `context.window_manager.keyconfigs.addon`.

  (blender/blender@4a6d687d53b42a6ce69ea324f59ee7202777056b).

### Pipeline & I/O

- `Mesh.calc_smooth_groups(use_bitflags=False, use_boundary_vertices_for_bitflags=False)`

  The Mesh API to compute smooth groups has been updated to also optionally take
  boundary vertices into account, and not only boundary edges.

  This seems to be the expected way to generate smooth groups by some other DCCs and
  game engines to correctly recompute normals from this data.

  See [!105516](https://projects.blender.org/blender/blender-addons/pulls/105516) and
  [!135248](https://projects.blender.org/blender/blender/pulls/135248) for details.

  Committed as
  [00de07f752d8c](https://projects.blender.org/blender/blender/commit/00de07f752d8c0ee44e2ea84a25e11e5fbcef96d).
