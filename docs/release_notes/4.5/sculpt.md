# Blender 4.5 LTS: Sculpt, Paint, Texture

* Node tools performance for simpler changes (when only positions, masks, or face sets were changed) has been improved significantly (blender/blender@0b891a68b101).
* `sculpt.mask_by_color` now takes in a `location` parameter to specify a region-space mouse position to execute the operator from a script (blender/blender@956a0fad80a5bc1bf203cab78d7a8598583e4342).
* *Mask by Color* operator added to the *Mask* menu and works with the redo panel (blender/blender@5b04b4ac189b2dd2da27db737252d9b2836974c2).