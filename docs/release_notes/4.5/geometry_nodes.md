# Blender 4.5 LTS: Geometry Nodes

* The *Mesh to Curve* node has a new "Face" option that converts each face to a cyclic curve (blender/blender@a48d155c8719).
* The new *Visual Geometry to Objects* operator converts procedurally generated instances to objects and collections that are editable as part of the scene's data (blender/blender@63e6f53ba06750).
* The instance geometries created by the *String to Curves* node are now given names based on their corresponding text character (blender/blender@3f596a651c60).
* The *Bounding Box* node now has an option for whether to include the radius of curves and point clouds (blender/blender@06f6d77979ce).
* Image inputs exposed to the modifier now have buttons to create or open an image instead of just selecting from existing images (blender/blender@f9e02b59250a4).
* The *Curve to Mesh* node now has a "Scale" input to scale the profile curve. This replaces the implicit use of the "radius" attribute that was used before. (blender/blender@a92b68939aca8a) 

### Performance
* Vertex to edge attribute domain interpolation is roughly 1.7x faster (blender/blender@33db2d372fc5).