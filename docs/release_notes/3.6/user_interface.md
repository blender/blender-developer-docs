# User Interface

- Tabs in the Properties editor show the name of the tab in a tooltip,
  quicker than the usual tooltips.
  (blender/blender@1b94e60fb0).
- Drag & drop inside Blender now supports
  <span class="hotkeybg"><span class="hotkey">Esc</span></span> or
  <span class="hotkeybg"><span class="hotkey" style="padding-left: 3px;">RMB
  ![](../../images/Template-RMB.png "../../images/Template-RMB.png")</span></span>
  to cancel dragging.
  (blender/blender@d58e422ac3).
- Hover highlighting of UIList rows.
  (blender/blender@7216eb8879).
- Some search boxes are widened if needed, for example if there is a
  linked scene.
  (blender/blender@cfb112edcb).
- Outliner now allows the filtering of Grease Pencil objects.
  (blender/blender@b558fff5b8).
- Resetting to defaults, eyedropper, and other operations working
  correctly in popovers.
  (blender/blender@c402b3f52f).
- The "Open Recent" menu list default size increased to 20 items.
  (blender/blender@19ca2e905e).
- Fallback font "Noto Emoji" updated with new Unicode 15.0 emojis.
  (blender/blender@2215ee9358).
- Color Picker can now adjust position to better fit within available
  space.
  (blender/blender@fa13058fa6).
- Searching for nodes with a slash character in their names works
  correctly now.
  (blender/blender@2b565c6bd0).
- Alt-click support to edit string properties for multiple objects.
  (blender/blender@43eb3fe21a).
- Better positioning of text with Measure/Ruler tools.
  (blender/blender@b367a2b5f9).
- Clicking the NLA/Graph Editor search box no longer scrolls the items
  below it.
  (blender/blender@d1219b727c).
- Node Editor supports Smooth View.
  (blender/blender@2f0b166dce).
- Support operator enums in Quick Favorites.
  (blender/blender@04d50f4b23).
- More Context Menus open with last-used option.
  (blender/blender@eac3d37ab1).
- Many Operator confirmations can be now be disabled with the "Confirm"
  checkbox (on by default) in the keymap.
  (blender/blender@6ae44c4770).

![](../../images/Keymap_confirm.png){style="width:600px;"}

## Asset Browser

- Assets now shows tooltips with the full name and description.
  (blender/blender@0ee0e8c0d4).

![](../../images/AssetTooltips.png){style="width:600px;"}

- Meta-data fields in the sidebar are not shown if blank and read-only.
  (blender/blender@199c7da06d).
- *Clear Asset* now works for all selected assets
  (blender/blender@20f54a5698).

## File Browser

- File Browser list mode items can now be dragged by the full name, not
  just by the icon.
  (blender/blender@c3dfe1e204).
- File Browser items now have "External" menu for opening and other
  operations outside of Blender.
  (blender/blender@694f792ee1).

## Platform-Specific Changes

- MacOS: recently opened/saved files will now show in Dock context menu
  and in App Expose.
  (blender/blender@f04a7a07e3).

![](../../images/MacDock.png)

- MacOS: double-clicking a blend file will now always load even if
  Blender loses focus while launching.
  (blender/blender@ed4374f089).
- Windows: autofocus child windows on hover.
  (blender/blender@defd95afcd).
- Windows: Image Editor can now copy/paste images with other
  applications.
  (blender/blender@39bcf6bdc9).

## 3D Text Objects

- Text selection by mouse dragging. Word selection by double-click.
  (blender/blender@68f8253c71).
- Nicer control and feedback for text styles.
  (blender/blender@f7ba61d3a6).
- "To Uppercase" and "To Lowercase" now supports most languages.
  (blender/blender@e369bf4a6d).
- Operators to move the cursor to the top and bottom.
  (blender/blender@6d2351d26b).

## Transform Settings

- Proportional Size can now be set in the Proportional Editing popover
  (blender/blender@6b5b777ca3).

![](../../images/ProportionalSize.png){style="width:600px;"}
