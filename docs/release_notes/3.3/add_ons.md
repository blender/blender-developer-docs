# Add-ons

## glTF 2.0

### Importer

- Add missing image name
  (blender/blender-addons@a2650c0158ee)
- **Manage some official Khronos Extensions about Materials**
  (blender/blender-addons@042fbefac686)

### Exporter

- **Manage some official Khronos Extensions about Materials**
  (blender/blender-addons@042fbefac686)
- Replace Separate RGB shader node by new Separate Color node
  (blender/blender-addons@c28c92c1494c)
- Allow custom name for merged animation
  (blender/blender-addons@501c97628f15)
- Export deformation bones only, without animation, is now possible
  (blender/blender-addons@807a64cdfc50)
- Draco: Directly look at path specified by environment variable
  (blender/blender-addons@11bc851e16ae)
- Fix camera & light export when Yup is off
  (blender/blender-addons@849e7196eb4e)
- Export all actions of a single armature is now under an export option
  (blender/blender-addons@09d752e84534)
- Fixed bezier control points to cubic spline tangents conversion
  (blender/blender-addons@08d13c024f60)
- **Manage all 4 types of Vertex Colors (corner/point - byte/float)**
  (blender/blender-addons@2fcac97522de)
- Performance: cache parent/children data to avoid bad `.children`
  performance when used a lot
  (blender/blender-addons@5b2953ad0883)
- Avoid crash when all drivers are invalids
  (blender/blender-addons@69b9725ba0f8)
- Add bufferView Target at export
  (blender/blender-addons@ef0027e72d29)

## Amaranth

- Support listing images used by Empty objects in Debug tool
  (blender/blender-addons@8d609fc7e68d)
- Add check for unsaved images in Save/Reload operator
  (blender/blender-addons@3c6a16fcbe5e)
- Fix Debug feature to list users of image datablocks
  (blender/blender-addons@4a522d353a58)

## Collection Manager

- Fix duplicate keymap entries for QVT operators.
  (blender/blender-addons@1cc09e24c516)
- Fix duplicate QCD widgets being added when pressing backspace while
  hovering over the QCD 3D Viewport Header Widget checkbox in the
  preferences.
  (blender/blender-addons@af8d747d00ac)
- Fix adding/removing objects from nested collections to/from the
  current selection.
  (blender/blender-addons@3570274fe75f)
