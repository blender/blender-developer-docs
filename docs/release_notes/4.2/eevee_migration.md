# EEVEE migration from older versions to Blender 4.2 LTS

In Blender 4.2 LTS, a major rewrite of EEVEE was released. While most of the features are kept compatible, some were completely rewritten and the compatibility is broken. This page explains the porting procedures and how to reproduce similar setup between both engine versions.

## World

- World volume now completely blocks distant light (world and sun lights) (blender/blender!114062). To keep the same appearance, the volume needs to be converted to a physical object. A conversion operator is provided in the top bar help menu or in the `World > Volume` panel. The operator will only appear if there is something to convert.

- World light now casts shadows from an extracted light position. When loading older files, `World > Settings > Sun > Shadow` is disabled to preserve their appearance.

## Shadows

- Previous settings for sun lights shadow resolution cannot be automatically converted, they are replaced by a new setting `Data > Light > Shadow > Resolution Limit` (see [documentation](https://docs.blender.org/manual/en/4.2/render/eevee/lighting.html)). If the previous resolution was much lower than the default value of the new setting, it might cause a huge performance penalty or trigger the `Shadow buffer full` error. Increasing the `Resolution Limit` will lower the resolution and fix the issue.
- `Render Settings > Shadow > Soft Shadows` is now a per light option `Data > Light > Shadow > Jitter`. This option is disabled by default since shadow map ray tracing can output plausible soft shadows at a much lower cost. If the shadow map ray tracing doesn't give satisfactory results, jittered shadows can be re-enabled on a per light basis.
- Jittered shadows are not visible by default in the viewport because of their cost. Turn on `Render Settings > Sampling > Viewport > Jittered Shadows` to enable them back.

- `Contact Shadows` is no longer available, since the accuracy of the new shadow maps is enough in most cases. For lights that were set up to rely heavily on `Contact Shadows`, the `Resolution Limit` might need to be lowered to avoid light leaking.

## Materials

- Material `Blend Mode` was replaced by `Render Method`. To reproduce the same behavior as the former `Alpha Clip` or `Opaque`, materials need be modified by adding a `Greater Than` Math mode. This is automatically handled for simple materials, but more complex setups need to be converted manually. See below for an example of how to port more complex mixing.

![Converting a complex alpha clip material to work with the new version of EEVEE](images/eevee_migration_alpha_clip.png)

- Material `Shadow Mode` was replaced by `Object > Visibility > Ray Visibility Shadow`. This new setting is turned off if all materials assigned to an object were using `Shadow Mode > None` option.
  - If `Shadow Mode` was `Alpha Clip`, it might have been automatically handled along side the `Blend Mode` compatibility. Otherwise the material node-tree might need similar adjustment.
  - If `Shadow Mode` was `Opaque`, the new `Material > Settings > Transparent Shadows` is turned off.
  - If `Shadow Mode` was `None`, the material node tree should have been automatically modified to disable shadows. See below for how to do it manually.

![Setup to disable shadow per material](images/eevee_migration_transparent_shadow.png)

- The introduction of the new see [Thickness workflow](https://docs.blender.org/manual/en/4.2/render/eevee/material/thickness.html) can affect materials using transmissive shader nodes.
  - Automatic conversion is done for `Refraction Depth` if the material was using `Screen Space Refraction`.
  - Translucent BSDF now uses a more accurate approximation when thickness is not zero. To reproduce the same results as in 4.1, a `Value` node with a value of 0 need to be connected to the `Thickness` socket of the material output node.
  - `Subsurface Translucency` has been removed as it is now always evaluated. However, it relies on the `Thickness` socket output to determine the inner absorption of the object. Shadow maps can still be use to refine this thickness by enabling `Material > Settings > Thickness From Shadow`. This can help achieve similar results as in 4.1.

- `Screen Space Refraction` and Light probe planes are no longer supported in materials using `Blended` render method (former `Alpha Blend`). The only workaround for now is to use `Dithered` render method and enable `Render Settings > Raytracing`. There are discussions adding new refraction option for these materials (blender/blender#122979).

## Render Settings

- Volumes now receive indirect light. When loading older files, `Clamping > Volume > Indirect Light` is set to near zero to preserve their appearance. To enable indirect light, reset the aforementioned property to the default value.

## Light Probes

- Light probe volumes samples location and influence volume have changed. They might need to be manually scaled up to avoid light leaking.
- Light probe volumes now have a `Surfel Resolution` option that might need some tweaking to be able to bake older scene (refer to the [manual](https://docs.blender.org/manual/en/4.2/render/eevee/light_probes/volume.html) for more detail).
- Light probe volumes object now contains the baked lighting. Baking needs to be performed again using `Scene > Light Probes > Bake All Light Probe Volumes`. Baking can also be performed on a per volume basis using `Data > Bake > Bake Light Cache`
- Light probe spheres option has been moved to `World > Settings > Light Probe` and `Scene > Light Probes`. Their meaning has change as they now correspond to square textures instead of cube textures.
- Light probe spheres and planes only record diffuse lighting. All specular & glossy lighting is converted to diffuse lighting. This is a limitation currently being investigated.
- Light probe visibility collections have been removed in favor of per object visibility option. Manual conversion is needed.

## Bloom

- The Bloom feature and its associated render pass has been removed in favor of the realtime compositor `Glare` node with `Bloom` glare type. Old files need to be manually adjusted by adding the `Glare` node in the compositor setup. The compositor result can be previewed in the viewport by enabling the compositor in the Shading pop-over.
