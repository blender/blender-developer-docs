# Python API & Text Editor

## Blender as a Python Module

Various changes have been made to improve support for running Blender as
a Python module.

- [bpy 3.4.0 is available on PyPi](https://pypi.org/project/bpy/), and
  can be installed through `pip install bpy`. Note that this requires
  Python 3.10 (matching Blender releases), other versions do not work.
- `bpy.app.program_path` defaults to an empty string which can be
  written to, allowing scripts to point to a Blender binary
  (blender/blender@f7a4ede79f9512f39db8632ff112e08a93f3a9d4).
- The module is now built as a self contained Python-package "bpy"
  (blender/blender@81558783e40394c2c60f61626eb6814f17128503).
- GPU rendering via (EEVEE / WorkBench) is now supported on Linux
  (blender/blender@3195a381200eb98e6add8b0504f701318186946d).

## Additions

- New GPU built-in shader enums that work for both 2D and 3D. The 2D and
  3D versions are deprecated.
  (blender/blender@8cfca8e1bd85)
- New function `bmesh_utils.bmesh_linked_uv_islands` to access UV
  islands from a BMesh
  (blender/blender@de570dc87ed17cae2d2d1ed4347793c440266b4b).
- File select dialogs will now call the `update` callbacks of the
  attached operator's `directory`, `filename` and `filepath`
  properties
  (blender/blender@48d7ff68f0df209c77bbb081ab46fbc109fd825a).
  This will allow for much more interactive dialogs since, for example,
  operators can now present different options based on what file is
  currently selected.

## Internal Mesh Format

The internal data structure for meshes has undergone significant
changes, mainly by splitting up data to be stored in separate arrays.
More information is available on the [a design
task](https://developer.blender.org/T95965). More mesh data is now
accessible with the generic attribute system, accessible with Python as
`mesh.attributes[name]`. The existing API to retrieve this data
remains, but it will be slower. The new methods should be faster in most
situations. The attributes may not exist, so they must be created first.

- The "hidden" status of Mesh vertices, edges, and polygons is now
  stored in separate generic boolean attributes
  (blender/blender@2480b55f216c31).
  - The attribute names are `.hide_vert`, `.hide_edge`, and
    `.hide_poly`, and have `BOOLEAN` type.
- Similarly, mesh selection has also been moved to generic attributes
  (blender/blender@12becbf0dffe).
  - The attribute names are `.select_vert`, `.select_edge`, and
    `.select_poly` and have `BOOLEAN` type.
- Mesh polygon material indices are now stored in a generic attribute
  (blender/blender@f1c0249f34c417).
  - The attribute name is `material_index`, with the `INT` type.
- Sculpt face sets are stored in a generic attribute, now accessible
  with the Python API
  (blender/blender@060a53414194).
  - The name is "`.sculpt_face_set`", with the `INT` type.
- Internally, bevel weights are now stored optionally in separate
  arrays, though the attribute for accessing them remains unchanged
  (blender/blender@291c313f80b4).
  - The `use_customdata_vertex_bevel` and
    `use_customdata_edge_bevel` properties have been replaced with
    operators for removing and adding the layers:
    `MESH_OT_customdata_bevel_weight_{vertex,edge}_{add,clear}`
- Subdivision surface edge creases are now stored optionally in a
  separate array
  (blender/blender@a8a454287a27).
  - Meshes have a new `edge_creases` property used to access crease
    values separately from edges (which is faster).
  - The properties `use_customdata_vertex_crease` and
    `use_customdata_edge_crease` have been removed. They can be
    replaced by the API above or the
    `MESH_OT_customdata_crease_{vertex,edge}_{add,clear}` operators.

As an example, here the sculpt face sets are created if they don't
exist.

``` python
if ".sculpt_face_set" not in mesh.attributes:
    face_sets = mesh.attributes.new(".sculpt_face_set", "INT", "FACE")
    face_sets.data[10] = 14
```

## Breaking Changes

- The unused node "add and link node" operator
  `NODE_OT_add_and_link_node` was removed
  (blender/blender@543ea415690f).
- Unused operators `MESH_OT_vertex_color_add`,
  `MESH_OT_vertex_color_remove`, `MESH_OT_sculpt_vertex_color_add`,
  and `MESH_OT_sculpt_vertex_color_remove` were removed.
  - These operators can be
    [replaced](https://developer.blender.org/rBAd0fa5bc86185a32acbf9ea79ea209d9d8a1f2935)
    with the `Mesh.vertex_colors.new()` and `.remove()` functions
    (which are also deprecated), or with the attribute API
    (`Mesh.attributes`).
- The `Mesh` properties `use_customdata_vertex_bevel`,
  `use_customdata_edge_bevel`, `use_customdata_vertex_bevel`, and
  `use_customdata_edge_crease` have been replaced with
  `has_bevel_weight_edge`, `has_bevel_weight_vertex`,
  `has_crease_edge`, and specific operators for adding and clearing
  the layer:
  `MESH_OT_customdata_bevel_weight_{vertex/edge}_{clear/add}`, and
  `MESH_OT_customdata_crease_{vertex,edge}_{add,clear}`
  (blender/blender@291c313f80b4cc,
  blender/blender@a8a454287a27).
- `Leak Size` for Grease Pencil Fill tool has been removed.
  (blender/blender@bdbf24772a0d)
- Added new methods to define how the lines are extender for Grease
  Pencil Fill tool
  (blender/blender@172b0ebe6adf).
- UV editing rounding mode, `Snap to Pixels` has been renamed `Round to Pixels`
  (blender/blender@b5115ed80f19).
- UV custom grid subdivisions can now be set for X and Y axis separately
  (blender/blender@a24fc6bbc1ae).
- Geometry nodes do not use the `NodeItem` system anymore
  (blender/blender@837144b4577f).
  - Functions like `nodeitems_utils.node_items_iter` will not retrieve
    them anymore. Instead, one should iterate over node types directly.
- Adding/removing/moving sockets on built-in node types is not possible
  anymore
  (blender/blender@52bd198153e).
- Nodes from newly created materials get their names translated if
  translation is enabled. They should not be referenced by name any more
  (blender/blender@9d732445b9).

### Render Engines Passes

Render results by default now only have a Combined pass, and render
engines need to explicitly specify all other passes they produce.
Blender queries the render engine's `update_render_passes` function
and adds all render passes that the engine specifies using
`register_pass`.
(blender/blender@3411a96e74)

Previously, a number of built-in render passes were automatically added
when the corresponding `ViewLayer.use_pass_\*` property was enabled.
If a render engine relied on this, the pass now needs to be explicitly
added in `update_render_passes`.
