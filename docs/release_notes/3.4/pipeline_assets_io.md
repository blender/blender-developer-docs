# Pipeline, Assets & I/O

## OBJ

- Support for "PBR" extensions in .mtl files
  (blender/blender@a99a62231e).
  This adds roughness, metallic, sheen, clearcoat, anisotropy,
  transmission parameters of Principled BSDF materials. Use "PBR
  Extensions" toggle in export settings to enable it.
- Global scaling factor was added to the OBJ importer
  (blender/blender@42b1a7d4c66).

## glTF 2.0

glTF I/O is an python addon, change logs can be retrieved on [Addon
page](add_ons.md)
