# Blender 2.92: Grease Pencil

## User Interface

- Interpolate tools now are visible in Draw mode and not only in Edit mode.
  When the tool is used in Draw mode, the `only selected strokes` option is disabled.
  (blender/blender@7bf0682aa98a)
- Enabled by default `Layer Onion Skin` for new layers.
  (blender/blender@ba740ad2abc6)
- New Material parameter to rotate texture for Dots and Strokes.
  (blender/blender@e6f61a4a379b)
  See also the new parameter in Texture modifier below.

![New rotation parameter](../../images/Gpencil_material_rot_param.png){style="width:600px;"}

- Recalculate Geometry operator added to Cleanup menu. This operator was
  only available using Search and it was used to fix old or damaged
  files.
  (blender/blender@756708627613)
- Cleanup menu reorganized.
  (blender/blender@216880bb4733)
- Target object parameter changed in Bake mesh animation and Trace
  image.
  (blender/blender@e9b955b99cd3)
- New menu Paint in Vertex Paint mode. These operators existed in
  previous versions, but the menu was removed by error. Also, now all
  operators support multiframe edition.
  (blender/blender@986955a2d53a)

![New Vertex Paint menu](../../images/GPencil_MenuVertex_Paint.png){style="width:600px;"}

- Added Pin icon for default eraser when using `Ctrl` key.
  (blender/blender@8e1b63d4bd5b)

## Operators

- New option to Bake Animation only for selected frames instead to bake
  all available frames. To bake only selected, select in the Dopesheet
  the keyframes to
  bake.(blender/blender@657e34435174)

![Bake selected frames](../../images/BakeAnimationFrame.png){style="width:600px;"}

- New option to trace image sequences and convert to grease pencil
  strokes. Now it's possible to trace a single image or a full sequence
  of images from an image sequence or a video. If the trace is for a
  sequence, a new batch job is started and a progress bar is displayed
  during the trace.
  (blender/blender@a5d237ac85c7)

<video src="../../../videos/Gpencil_trace_seguences.mp4" title="Trace Sequences" width="600" controls=""></video>

- Improved Join operator to use the distance between points. Before, the
  join was done using the order of the strokes but not the distance
  between extremes. Now, the algorithm calculates the distance and tries
  to join always the nearest extremes.
  (blender/blender@21f201e25e70)

![New Join algorithm](../../images/Gpencil_before_after_join.png){style="width:600px;"}

- Improved method for interpolation. The new method gives better results
  when interpolating between different sized strokes. Previously, the
  longer stroke would be cut off. With the new algorithm, the strokes
  are properly "stretched" to fit the extreme shapes exactly.
  (blender/blender@151e847b8709)

<figure>
<video src="../../../videos/Gpencil_before_after_interpolation_method.mp4" title="New interpolation method" width="600" controls=""></video>
<figcaption>New interpolation method</figcaption>
</figure>

- New operator in Vertex Paint mode to reset vertex color data. After
  running this operator, all vertex paint data is removed and the stroke
  is reset to default material status.
  (blender/blender@9081b80d15b3)
- Now Merge Layers respects the old keyframe drawing when the target
  frame was missing. See Task
  [T83705](https://developer.blender.org/T83705) for more details.
  (blender/blender@57f900e4efda)

## Tools

- New option to set flat the cut extreme of the stroke after using
  Cutter tool. If the stroke angle is very extreme, parts of the stroke
  can be visible still.
  (blender/blender@2985a745bb01)

![Cutter Caps](../../images/Cutter_Flat_Option.png){style="width:600px;"}

- New option to automerge strokes while drawing.
  (blender/blender@e9607f45d85d)

## Modifiers and VFX

- Now Subdivision modifier adds geometry if the stroke is using cyclic.
  This new geometry only refers to number of points, but the extremes of
  the stroke are not smoothed at all in order to preserve original
  extreme points.
  (blender/blender@e296d58cf220)
- New parameter to rotate textures in Dots and Strokes in Texture
  modifier. This effect could be also obtained using the new material
  texture rotation parameter.
  (blender/blender@c80594f57f1e)
- Now the Effects can be linked using `Ctrl + L` Link operator.
  (blender/blender@c8a3d1a1fa65)
- New Additional menu to Copy Effects.
  (blender/blender@6327771bc968)

![Additional Menu](../../images/Gpencil_Effects_Additional_Menu.png){style="width:600px;"}

- Array modifier now support uniform random scale.
  (blender/blender@5535b0b8878a)

## Curve edit mode

- The Google Summer of Code project [Editing grease pencil strokes using
  curves](https://archive.blender.org/wiki/2024/wiki/User:Filedescriptor/GSoC_2020/Report.html) added a new
  submode in edit mode (similar to multi-frame edit) that allows the
  transformation of strokes using bézier curves.
  (blender/blender@0be88c7d15d2)

<figure>
<video src="../../../videos/Curve_edit_mode_demo.webm" title="Curve edit mode" width="600" controls=""></video>
<figcaption>Curve edit mode</figcaption>
</figure>

- To enable Bezier mode, use the bezier button in the top bar or press
  `U` key.

<!-- -->

- Note: all the edit mode operators are supported **except** the ones
  below, which will be added soon:
  - Duplicate
  - Copy & Paste
  - Snap to Cursor & Cursor to selected
  - Flip
  - Simplify & Simplify Fixed
  - Trim
  - Separate
  - Split
