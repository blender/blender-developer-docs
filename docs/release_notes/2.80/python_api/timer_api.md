# Blender 2.80: Timer API

Use cases:

- execute a function every x seconds
- execute a function in x seconds
- execute a function multiple times with different delays in between

Checkout the API documentation for more details:
<https://docs.blender.org/api/blender2.8/bpy.app.timers.html>
