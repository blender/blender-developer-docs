# Blender 3.0: Cycles

## GPU Rendering

GPU kernels and scheduling have been rewritten for better performance,
with rendering between 2x and 8x faster in real-world scenes.

### NVIDIA

The hardware minimum requirements have remained the same, however NVIDIA
driver must be version 470 or newer now.

OptiX has the following improvements:

- Rendering of hair as 3D curves (instead of ribbons) is significantly
  faster, by using the native OptiX curve support
- Baking now supports hardware ray-tracing
- Runtime kernel compilation time was significantly reduced, in many
  scenes it is less than a second now

![Render time with an NVIDIA Quadro RTX A6000 and OptiX](../../images/Cycles_3.0_optix_render_time.png){style="width:800px;"}
![](../../images/Cycles_3.0_benchmark_images.png){style="width:800px;"}

### AMD

For AMD GPUs, there is a new backend based on the [HIP
platform](https://github.com/ROCm-Developer-Tools/HIP). In Blender 3.0,
this is supported on Windows with RDNA and RDNA2 generation discrete
graphics cards. It includes Radeon RX 5000 and RX 6000 series, and
Radeon Pro W6000 series GPUs. Driver Radeon Pro 21.Q4 or Adrenalin
21.12.1 (or newer) is required.

We are working with AMD to add support for Linux and investigate earlier
generation graphics cards, for the Blender 3.1 release.

Note that in the AMD driver, HIP is only officially supported for RDNA2
graphics cards. However we have found that RDNA also works in practice.

![Render time with an AMD Radeon RX 5500 XT and HIP](../../images/Cycles_3.0_hip_render_time.png){style="width:800px;"}

Benefits of the new HIP backend include:

- Reduced render time
- No runtime kernel compilation, so renders start quickly
- Feature parity with NVIDIA GPUs

### Apple

Support for GPU rendering through Metal is under development for Blender
3.1.

### OpenCL

OpenCL rendering support was removed. The combination of the limited
Cycles kernel implementation, driver bugs, and stalled OpenCL standard
has made maintenance too difficult.

We are working with hardware vendors to bring back GPU rendering support
using other APIs.

## Viewport Interactivity

Viewport rendering is significantly more responsive due to new
scheduling and display algorithms.

<figure>
<video src="../../../videos/Blender_3_0_cycles_navigation_2.mp4" title="Viewport interactivity compared in Blender 2.93 and 3.0. Demo by Statix / redhoot" width="800" controls=""></video>
<figcaption>Viewport interactivity compared in Blender 2.93 and 3.0. Demo by Statix / redhoot</figcaption>
</figure>

## Sampling

The recommended workflow is now to set a noise threshold (instead of
number of samples), and let Cycles adaptively refine the image until the
denoiser can eliminate the remaining noise. The default sampling
settings and user interface are geared towards this workflow, though it
is not required.

More specifically:

- Redesigned sampling user interface and default settings.
- Improved adaptive sampling. Noise threshold behavior changed, it
  needs to be manually tweaked to get comparable results to older
  versions. [See manual](https://docs.blender.org/manual/en/3.0/render/cycles/render_settings/sampling.html#adaptive-sampling).
- Time-based rendering limit. [See manual](https://docs.blender.org/manual/en/3.0/render/cycles/render_settings/sampling.html#bpy-types-cyclesrendersettings-time-limit).
- Separate presets for viewport and render sampling.

### Scrambling Distance

A new Scrambling Distance setting was added to the Advanced panel which
can be used to control the amount of correlation between pixels with a
range from 0 (highly correlated, not recommended) to 1 (no correlation).
This can decrease render times for GPU rendering. To automatically
estimate a good value, there is an Automatic setting. This automatically
configures the scrambling distance and is the recommended way to use
this setting. [See manual](https://docs.blender.org/manual/en/3.0/render/cycles/render_settings/sampling.html#bpy-types-cyclesrendersettings-scrambling-distance).
(blender/blender@366262bef542,
blender/blender@82cf25dfbfad)

## Denoising

OpenImageDenoise was upgraded to version 1.4, with improved detail
preservation. A new prefilter option was added to control the denoising
of auxiliary albedo and normal passes.
(blender/blender@c5e5ac4)

Auxiliary albedo and normal passes for volumes were improved, to better
preserve volume detail and reduce low frequency noise.
(blender/blender@d18a3b7)

|Before|After (Renders by Jesus Sandoval)|
|-|-|
|![](../../images/Cycles_3.0_volume_denoise_before.jpeg)|![](../../images/Cycles_3.0_volume_denoise_after.jpeg)|
|![](../../images/Cycles_3.0_archviz_denoise_before.jpeg)|![](../../images/Cycles_3.0_archviz_denoise_after.jpeg)|



## Tiles & Memory Usage

The render is now progressively refined by default, giving a quicker
preview of full render. However when rendering high resolution images
with many render passes, memory usage can be significant. For this
reason high resolution renders are automatically split into large tiles,
and each tile is cached to disk as it is completed.

Previously tweaking tile size was important for maximizing CPU and GPU
performance. Now this is mostly automated. There are still two
situations where reducing tile size from the default can be helpful:

- When there is not enough memory to fit both the scene and render
  result, reducing tile size makes more memory available for the scene.
- When a small object renders very slowly compared to other objects (for
  example transparent hair), rendering with smaller tiles can help keep
  the GPU busy.

## Shadow Catcher

The shadow catcher was rewritten. New features include:

- Indirect and environment light support, for more accurate compositing
- Option for lights to be considered included or excluded, that is if
  they are real or synthetic objects

By default the shadow catcher only affects the alpha channel of the
Combined pass. However to fully handle colored indirect light and
emission, the new Shadow Catcher pass should be used. The background
should be multiplied by the Shadow Catcher pass, after which the
Combined pass can be composited over it with alpha over.

|Background|Shadow Catcher|Combined|Composited|
|-|-|-|-|
|![](../../images/Cycles_3.0_shadow_catcher_background.jpg)|![](../../images/Cycles_3.0_shadow_catcher_pass.jpg)|![](../../images/Cycles_3.0_shadow_catcher_combined.jpg)|![](../../images/Cycles_3.0_shadow_catcher_composited.jpg)|

![Shadow catcher passes and compositing setup.<br/>Renders by Joni Mercado.](../../images/Cycles_3.0_shadow_catcher_compositing.png)

## Shadow Terminator

New option to reduce shadow artifacts that often happen with low poly
game models. Offset rays from the flat surface to match where they would
be for a smooth surface as specified by the normals.
(blender/blender@9c6a382)

The Geometry Offset option works as follows:

- 0: disabled
- 0.001: only terminated triangles (normal points to the light, geometry
  doesn't) are affected
- 0.1 (default): triangles at grazing angles are affected, and the
  effect fades out
- 1: all triangles are affected

Generally, using one or a few levels of subdivision can get rid of
artifacts faster than before.

![Low poly rendering compared between Blender 2.93 and 3.0. Render by Juan Gea Rodriguez.](../../images/Cycles_3.0_shadow_terminator.png)

## Subsurface Scattering

Subsurface scattering now supports anisotropy and index of refraction
for Random Walk. Realistic skin has an anisotropy around 0.8.
(blender/blender@7811c11)

|Anisotropy 0.0|Anisotropy 0.8 (Renders by Metin Seven)|
|-|-|
|![](../../images/Cycles_3.0_zombie_anisotropy_0.0.png)|![](../../images/Cycles_3.0_zombie_anisotropy_0.8.png)|

## More Features

- Light option to be visible to camera rays
- New Position render pass
- Volume sampling quality is now the same for CPU and GPU rendering
- Baking support for denoising and adaptive sampling
- Light clamping was modified to be more fine grained, preserving more
  non-noisy light.
- Open Shading Language metadata support for UI labels and checkboxes,
  following the specification.
  (blender/blender@e6bbbd9)

    [[ string label = "UI Label" ]]
    [[ string widget = "checkBox" ]]
    [[ string widget = "boolean" ]]

## Removed Features and Compatibility

- Branched path tracing was removed. We recommend using adaptive
  sampling to automatically allocate more samples where needed.
- Cubic and Gaussian subsurface scattering. The physically based Random
  Walk and Christensen-Burley methods remain.
- The CMJ sampling pattern was removed. Only Sobol and PMJ remain.
- The NLM denoiser was removed. We recommend using OpenImageDenoise
  instead for final frames.
