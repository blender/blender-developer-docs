# Blender 2.79b: Bug Fixes

Changes from revision
blender/blender@9accd8852b11
to
blender/blender@f4dc9f9d68bd,
inclusive.

Total fixed bugs: 12 (7 from tracker, 5 reported/found by other ways).

**Note:** Nearly all commits since 2.79a are listed here, only really
technical/non-user affecting ones have been skipped.

- Fix [\#54003](http://developer.blender.org/T54003): Particles - Size
  and random size not present in "physics" tab
  (blender/blender@a3409d3f53f1d).
- Fix [\#53478](http://developer.blender.org/T53478),
  [\#53430](http://developer.blender.org/T53430): Sequencer cut edge
  case fails
  (blender/blender@f3018322c097f).
- Fix [\#54204](http://developer.blender.org/T54204): Wrong selection on
  Clean Tracks (Motion Tracking)
  (blender/blender@caa0b0fadf928).
- Fix [\#54206](http://developer.blender.org/T54206): bevel and inset
  operations repeat did not remember offset
  (blender/blender@596f33f801f8).
- Fix [\#54348](http://developer.blender.org/T54348): Bone dissolve
  gives invalid hierarchy
  (blender/blender@865fbe343a906).
- Fix [\#54234](http://developer.blender.org/T54234): add
  BLENDER_VERSION_CHAR to .plist
  (blender/blender@b5b5260464f).
- Fix [\#54360](http://developer.blender.org/T54360): FFMPEG bitrate not
  editable for all codecs
  (blender/blender@f4dc9f9d68b).

<!-- -->

- Fix (unreported): Fix Error passing context arg to marker menu
  (blender/blender@807663742b7bc).
- Fix (unreported): Fixed: cache clearing when using multiple Alembic
  files
  (blender/blender@09c88fed1f5).
- Fix (unreported): Fix bone dissolve using wrong envelope radius
  (blender/blender@ce51066e47175).
- Fix (unreported): Tracking: Warn when no tracks are selected when
  creating mesh
  (blender/blender@658fb7f4536).
- Fix (unreported): Tracking: Make object created form tracks active and
  selected
  (blender/blender@23ffd4ec394).

## Addons:

Fix [\#53766](http://developer.blender.org/T53766): MeasureIt
measurements not appearing on all layers an object exists
(blender/blender-addons@dc6704ab4231).
