# Blender 4.3: User Interface

## General

- Hover tooltips for the Data-block ID selector shows previews for images, movies, and fonts.
  (blender/blender@8937c0bcab)

![ID Selector Tooltips](images/IDSelectorTooltips.png)

- Tool-tips are never resized with 2D region zooming.
  (blender/blender@1b6910c61c)
- Color picking from the viewport in Rendering mode now returns the linear color value at that
  location, without the view transform being applied.
  (blender/blender!123408)
- "Save Incremental" menu item moved down in its section to reduce accidental selection.
  (blender/blender@9c4e56e307)
- Windows with a single editor area now show descriptive titles.
  (blender/blender@554400da9a, blender/blender@47223553f7, blender/blender@503cb7bfee)

![Window Titles](images/WindowTitles.png)

- Maximum resolution scale increased. Interactive to 3.0X, non-interactive to 6.0X.
  (blender/blender@cc9bd758c9)

![Resolution Scale Increased](images/ResolutionScale.png)

- Text weights change correctly if selecting a UI font with non-regular weight.
  (blender/blender@1992296828)
- "Widget Label" Text Style removed.
  (blender/blender@108b71047a)
- New Text Style added for tool-tips.
  (blender/blender@5bf44c6eb4)

![Tooltip Text Style](images/TooltipTextStyle.png)

- Custom platform-specific mouse cursors added for VSE handle manipulation.
  (blender/blender@459572b15f, blender/blender@13795b5df1, blender/blender@8199da2c1e)

![VSE Cursors](images/VSECursors.png)

- Tooltips no longer always append "." to the end of all items.
  (blender/blender@f2f408bebf, blender/blender@bfa36039c2, blender/blender@7b0c18afb4)
- Padding added to UI List and tree items. (blender/blender@d30d8b4bfa)
- Scroll padding added to VSE & NLA redo panels.
  (blender/blender@a1338312af)
- File Browser thumbnail file type icon moved to corner of item bounds.
  (blender/blender@b64cf71c1d)
- "LTS" is added to title bar version string if applicable.
  (blender/blender@7374077610, blender/blender@48d55fb420)
- Custom Curve control points are now larger.
  (blender/blender@90e7331d10, blender/blender@885824540a)
- Asset Browser: Rename the "All" libraries drop-down entry to "All Libraries" for clarity.
  (blender/blender@b885562ba7)
- Color sampling performance improvements. (blender/blender@d8f550875b)
- Save Copy operation no longer clears the current file's "dirty" status.
  (blender/blender@01d5581506)
- Autokeying indicator is now colored red when enabled. (blender/blender@aa78d43a05)

![Red Autokeying Button](images/AutoKeyingRed.png)

- Hit size for resizing areas increased. (blender/blender@b04c0da6f0)

![Area Edges](images/AreaEdges.png)

- Timeline (and similar editors) hide the scroll-bars when they are sized very short.
  (blender/blender@f40cd831fd)

![Slim Timeline](images/TimelineSlim.png)

- Option to clear only items not found from the Recent Files list.
  (blender/blender@4317f50f5f)
- Items not found on the Recent Files list are no longer removed automatically.
  (blender/blender@5bb7f06409)

![Removing Recent Not Found](images/RecentNotFound.png)

- Dialogs now show a different cursor at the top to indicate they can be dragged.
  (blender/blender@0e690d3abb, blender/blender@442fd1925e, blender/blender@a5779347b6)
- ID Search lists increased in width. (blender/blender@019d987072)
- Color Picker revamped. (blender/blender@5cfe733f84)

![Color Picker Revamp](images/ColorPickerRevamp.png)

- Editor edge highlighting to indicate active area.
  (blender/blender@28e5c1412e, blender/blender@89d7f00ff8, blender/blender@44168c32ff)

![Editor Edge Movement](images/AreaHighlight.png)

- Highlight feedback when dragging editor edges. (blender/blender@b2eb8cc0df)

![Editor Area Highlight](images/EdgeMovement.png)

## Icons

- All small UI icons now being used directly as separate SVG files, rendered to any size.
  (blender/blender@cc1f7ef983, blender/blender@5c377686e7, blender/blender@21f2d91359,
  blender/blender@c3377a3498, blender/blender@6be90285bc, blender/blender@445b609e2d,
  blender/blender@9ee86c589d)

![Large Icons](images/IconsLarge.png)

- Dialogs now using SVG icons directly.
  (blender/blender@f715f3853f, blender/blender@983b0b6182, blender/blender@983b0b6182)
- File Browser now using SVG icons directly.
  (blender/blender@1d47e3276b, blender/blender@4d84940253, blender/blender@7045fb6593,
  blender/blender@d9971d6414)

![File Browser Icons](images/FileBrowserIcons.png)

- About dialog using new SVG system for the Blender logo.
  (blender/blender@b7256b1ea7)
- Icon design improvements.
  (blender/blender@bea0c5c914, blender/blender@bfc7e2aea2, blender/blender@ae71f33c75,
  blender/blender@989fcc2c4f, blender/blender@7c68ac258f)
- Now using an unaltered version of the Blender logo throughout.
  (blender/blender@c46831949, blender/blender@cfe77fbafd, blender/blender@5285d7b859)
- Support for full-color icons.
  (blender/blender@6145920a0d, blender/blender@b63d691570, blender/blender@30038f1710,
  blender/blender@99a4bc325e)
- Multicolor icons can have internal part colors changed to match theme.
  (blender/blender@cd998d392d, blender/blender@bc43d243bb)
- New geometry set icon.
  (blender/blender@ae129da4f)
- Splash screen now shows version in white, even in light themes.
  (blender/blender@fb651c1613)

## Status Bar
- New SVG icons now used on status bar.
  (blender/blender@a57c3be05e, blender/blender@5fb140a36e, blender/blender@2859eabd1c)
- Status bar cleared during text input and other modal operations.
  (blender/blender@0c1d8ad8a5)

![Status Bar Icons](images/StatusBarIcons.png)

- Improved Spacing.
  (blender/blender@fe4c7c4178, blender/blender@24e6b33942, blender/blender@eba9771ef2)
- More operators using the new system.
  (blender/blender@f966bb9ed4b661359784ae86dc1383fd82e6cee2,
  blender/blender@c3243f363d45123fc43ccdbcf5c00f43d5ebb493)
- Screen operation keymaps info now shown on the Status Bar.
  (blender/blender@5642944e33, blender/blender@223d68a733)

## Area Docking
- New ability to move editor areas to any other area by dragging
  (blender/blender@e802fe1433, blender/blender@1404a95dde, blender/blender@f238b44dee,
  blender/blender@73e519ded2,
  blender/blender@2c4a611be1, blender/blender@f6ae3bf8f3, blender/blender@a9f9062df2,
  blender/blender@223d68a733, blender/blender@8f996f1786, blender/blender@ae3bf84f35,
  blender/blender@1d1ea87617, blender/blender@810687ad5b, blender/blender@6792d6dfbe,
  blender/blender@f08ade0523, blender/blender@04652334f3, blender/blender@c30d7bb458,
  blender/blender@9665b32320, blender/blender@a0df365b77, blender/blender@382244c258,
  blender/blender@aa5385f648, blender/blender@8450aa275f, blender/blender@bfe693c8c2)

![Docking](images/DockingMove.gif)
  
![Docking](images/DockingFloat.gif)

## Spreadsheet
- Support for selecting the viewed geometry from the instance tree. (blender/blender@071b18a3cc)

## Keymap
- Assign mouse buttons 4 and 5 to navigate back/next in File Browser. (blender/blender@095aab986b)

## Nodes

* Node groups can now have a default node width (blender/blender@e842966c5e).
* Built-in nodes can have line separators now
  (blender/blender@246c513f92353af7c9d1644901ce160da83e928a).
* Panel descriptions show up when hovering over them now
  (blender/blender@c0179a1b782eb8d7f298421f4adae04307aa9d02).
* String sockets can now use placeholders instead of labels to make the input field wider
  (blender/blender@a6d5652043e4b98035a535c693bdf7ae868053ae).
* Inserting nodes with link-drag-search is much more convenient now
  (blender/blender@83fa565ec28f8faae7da0057a086f1ff481c1a39).


## Platforms

- macOS : New "Hand"-style mouse cursors added for dialog and docking interaction.
  (blender/blender@458c60269b, blender/blender@72a0be0944)

![Hand Mouse Cursors](images/HandCursors.png)

- macOS: Image copy/paste support in the Image Editor
  (blender/blender@92d00678ac9cc95a19ebff4c5999420df8dcf972)
