# Blender 4.3: Rendering

## Shading

### Metallic BSDF

A new Metallic BSDF node has been added to the shader editor.
(blender/blender@6ccb33e9fe14a5c312348dbd3deab37ad3057ca9)

- The Metallic BSDF exposes existing,
but hard to access metallic material configurations in a small node.
These configurations are:
  - The F82 Tint Conductor Fresnel approximation, as is currently in use by the Principled BSDF.
  - A Conductor Fresnel, previously limited to custom OSL scripts.
- The F82 Tint approximation is artist friendly, taking colors as inputs.
- The Conductor Fresnel is harder to use, taking IOR and Extinction coefficients
(sometimes known as \(n, k\)) per color channel as inputs.
However it can produce subtly more accurate results for real world metals.
- EEVEE does not support the Conductor Fresnel type,
so it internally remaps the IOR and Extinction coefficients
to colors and uses them with the F82 Tint approximation.

  ![Metallic BSDF recreating Nickel and Copper materials](images/shader_metallic_bsdf_example.jpg)

## Texturing

- A new *Gabor Noise* texture node was added, which can be used to create random interleaved bands
  whose direction and width can be controlled. See
  [documentation](https://docs.blender.org/manual/en/4.3/render/shader_nodes/textures/gabor.html).
  (blender/blender@4f51033708)

![Gabor Noise Example](images/gaborNoiseExample.png)

## Color Management

- The view transform now supports white-balancing the displayed image,
  mapping colors such that a given white point
  (expressed in color temperature and tint) ends up as white on the display.
  (blender/blender!123278)
