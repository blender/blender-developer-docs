# Blender 4.3: Core

## Data-Block Renaming

The [behavior of ID renaming from the user
interface](https://docs.blender.org/manual/en/4.3/files/data_blocks.html#name-rename)
has been modified, according to the design defined in blender/blender#119139.
Under some specific conditions, it is now possible to enforce the chosen name on the renamed ID,
even if it collide with another data-block (the other one will also be renamed then)
(blender/blender@3e03576b09).
