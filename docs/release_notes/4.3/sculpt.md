# Blender 4.3: Sculpt, Paint, Texture

With over a hundred default brushes and an improved selection interface,
Blender’s new asset libraries make brush management and customization simpler
than ever—and allow brushes to be shared across projects or with others.

## New Brush Management Workflow

Brushes for all sculpt and paint modes are now managed through asset libraries,
allowing them to be shared across projects. Updating and extending these libraries
with custom brushes is easily done from any blend file. The brush selection
interface has been redesigned to provide faster and more convenient access to
brushes in your asset libraries.

The *Essentials* asset library includes 130 default brush assets, each with a
carefully designed thumbnail in a consistent style. Custom brushes can be created
by duplicating an existing built-in brush. Each brush is saved in its own blend
file within the chosen asset library, with textures and other dependencies packed.

Refer to the
[user manual](https://docs.blender.org/manual/en/4.3/sculpt_paint/brush/introduction.html)
to learn more about the new asset based workflow.

### Accessing Brush Assets

The [asset shelf](https://docs.blender.org/manual/en/4.3/interface/window_system/regions.html#asset-shelf)
is now the primary location for selecting brushes, replacing the toolbar.
Brushes are organized into asset catalogs rather than being grouped by tool.

The asset shelf appears horizontally at the bottom of the 3D Viewport and Image Editor
in sculpt and paint modes. It can be configured to display a subset of catalogs,
based on the needs of the project.

![Brush Asset Shelf](images/sculpt-paint_brush_introduction_brush-asset-shelf.png)

Alternatively, brushes can be selected in a popup. This can be accessed by clicking
on the brush thumbnail in the tool settings or via the shortcut `Shift Spacebar`.

![Brush Asset Popup](images/sculpt-paint_brush_introduction_brush-asset-popup.png)


### Converting Brushes

In case you want to convert an existing brush in a file to a brush asset that you can access,
the easiest way is to follow these steps:

1. Set the Outliner to the Display Mode 'Blender File' to see all data in the current file
2. Search for your brush, right click on it and 'Mark as Asset'
3. A new brush will appear in your Asset Shelf in the paint/sculpt mode it belongs to
4. Right click on it and use 'Save As Asset' to save it to an asset library and catalog of your choosing
5. Make sure to right click on the local brush in the Outliner again to 'Clear Asset'

Custom brush assets are typically saved to the User Library.
By default the folder of this library can be found in `Documents/Blender/Assets`.
Then in the outliner with Blender File view, right click each brush and Mark as Asset.

Each brush that is saved this way is stored as an `.asset.blend` file which can be freely edited from anywhere.
For more information on saving and managing brush assets, see the [user manual](https://docs.blender.org/manual/en/4.3/sculpt_paint/brush/brush_management.html).


### Redesigned Brush Thumbnails

All brush thumbnails, across all modes and object types, have been updated to follow a consistent new style.
To learn how these were created and how to create your own, we added [a detailed style guide](https://developer.blender.org/docs/features/interface/asset_thumbnails/).

![Brushes bundled with the Essentials asset library](images/sculpt_paint_essentials_brush_assets.png)

### Brush Updates

Apart from the new brush thumbnails there have been various edits to existing brushes (blender/blender-assets@9dfd0a171f412e4fec900383831810106e977b4f)

- **Mask** = Increased hardness, 1.0 strength
- **Multiplane Scrape** = “Plane Angle” set to 0, decreased normal radius
- **Density** = Added auto-smooth to
- **Clay Strips** = Overhauled based on community feedback
- **Clay** = Tweaked auto-smooth, disabled radius pressure sensitivity
- **Nudge & Thumb** = Lowered Normal Radius to detect surface direction better
- **Relax Slide** = Smaller stroke spacing
- **Crease** = Renamed to Crease Polish to differentiate it with other brushes

### New Brushes

Various new brushes were introduced for mesh objects to make the selection of basic brushes more complete (blender/blender-assets@9dfd0a171f412e4fec900383831810106e977b4f)

- Painting (Texture, Vertex & Sculpt):
  - Erase Hard, Erase Soft, Erase Hard Pressure,
  - Paint Hard, Paint Soft, Paint Hard Pressure, Paint Soft Pressure, Paint Square, Paint Blend,
  - Blend Hard, Blend Soft, Blend Square
  - Airbrush, Sharpen
- General Sculpting:
  - Grab 2D, Grab Silhouette
  - Crease Sharp, Elastic Snake Hook, Pull, Plateau, Trim, Relax Pinch
- Cloth Sculpting - The cloth sculpting brushes have been overhauled into various brushes:
  - Pinch Folds Cloth, Pinch Point Cloth
  - Grab Cloth, Grab Planar Cloth, Grab Random Cloth
  - Drag Cloth, Push Cloth, Inflate Cloth, Expand/Contract Cloth, Bend/Twist Cloth, Stretch/Move Cloth, Bend Boundary Cloth, Twist Boundary Cloth
  
### Python API

With the change to the asset-based system for brushes, the `paint.brush_set` operator has been removed. An equivalent example
of setting the active brush to the built-in "Blob" brush can be seen below.

```python
bpy.ops.brush.asset_activate(asset_library_type='ESSENTIALS', asset_library_identifier="", relative_asset_identifier="brushes/essentials_brushes-mesh_sculpt.blend/Brush/Blob")
```

## Performance

Brush evaluation, viewport drawing, and many other parts of sculpt mode were almost completely
rewritten to solve problems, improve performance, and ease future development.
See the [blog post](https://code.blender.org/2024/11/this-summers-sculpt-mode-refactor/) for more details.
A full list of the work is available on the
[development task](https://projects.blender.org/blender/blender/issues/118145).

- Entering sculpt mode is about 5 times faster
  (a change from 11 to 1.9 seconds with a 16 million face mesh with a Ryzen 7950x).
- Brush evaluation is about 8 times faster for mesh sculpting
  (a benchmark change from 2.9 to 0.36 seconds sculpting most of a 6 million face mesh).

## General
- The Voxel Size operator now works in relative mode by default.
  (blender/blender@4221f827cf1b7c25c0cb2b47cca754ec11540fa9)
- The lasso tools now have optional stroke stabilization
  ([manual](https://docs.blender.org/manual/en/4.3/sculpt_paint/sculpting/introduction/gesture_tools.html#tool-settings))
  (blender/blender@9d4d1aea98e1b30278a337f440f9962afe35bf2a).
- A poly-line gesture can now be finished with double-click
  (blender/blender@64211aa8c61da7473c5ff78f405df5aaafb2c195).
- Sculpt mask is now accessible in node tools (blender/blender@98f0926825d7).

## New Tools
- *Mask from Boundary* operator added to modify mask values based on mesh or face set islands
  ([manual](https://docs.blender.org/manual/en/4.3/sculpt_paint/sculpting/editing/mask.html#mask-from-mesh-boundary))
  (blender/blender@591f4c0e59dd132f3ee3a74cb1c3908bdd58016c).
