# Blender 4.0: Core

## Blend File Compatibility

- The [new blendfile compatibility
  policy](../../handbook/guidelines/compatibility_handling_for_blend_files.md) has been
  implemented.
  (blender/blender#109151,
  blender/blender!110109)
- Very early 2.80 development 'scene collection' compatibility code as
  been removed. This does not affect any file produced by a released
  version of Blender.
  (blender/blender#110918,
  blender/blender!110926,
  blender/blender@23835a393c)

## Data-Blocks

- Custom Properties can now be defined from the UI to store references
  to other data-blocks
  (blender/blender@b3c7f3c8a9).
  Previously, this was only doable through Python API.

## Library Overrides

- Many fixes and improvements were made, as part of the [Blender
  Studio's "WING
  IT!"](https://www.youtube.com/watch?v=u9lj-c29dxI&pp=ygUPd2luZyBpdCBibGVuZGVy)
  production. (blender/blender!109704)
  - Parenting-related Object properties (the parent inverse matrix,
    parent type, parent sub-target) will now be reset to the linked
    reference data when parent pointer itself is modified in library
    file and updated during resync process
    (blender/blender@9ed5177055,
    blender/blender@4842424220).
  - Cleanup-code pruning unused liboverride operations during RNA
    diffing would not always work properly, leading to accumulated
    invalid operations that could, under certain circumstances, end up
    severely breaking especially collections/objects hierarchies of
    liboverrides
    (blender/blender@784d09a87c).
  - Resync Enforce tool would not always work properly with collections
    of IDs (affecting especially Collections/Objects relationships)
    (blender/blender@2dfbd653a0,
    blender/blender@1c0ffa1e1816a5).
  - Code trying to fix invalid liboverride hierarchies was
    over-enthusiastic, which could pull data out of their valid original
    hierarchy into some more local, undesired one when production files
    start to become messy
    (blender/blender@bf93fb0f46).
  - Handling of lookup of items in RNA collections has been improved, to
    alleviate issues when there are name collisions between items
    (blender/blender@a05419f18b).
  - As a second approach to work around issue described above, changes
    were made to liboverride IDs naming, such that when created, they
    either get the exact same name as their linked reference, or they
    get a name that does not collide with any other linked override
    reference
    (blender/blender@b9becc47de,
    blender/blender@e11da03e7a).

## Breaking Changes

- The Depth pass support was removed from OpenEXR (the non-multilayer
  case) and Iris
  (blender/blender@e1b60fdb913ebe60a57e2459f0d1fbc1f921e643)
- Unused linked data is no longer kept when saving and re-opening a
  blendfile. This was already partially the case in previous releases.
  - The 'Fake User' setting is now fully irrelevant for linked data, and
    systematically cleared on load or link.
  - When an unused (as in, having zero users) linked data-block needs to
    be kept in a blendfile, users are expected to reference them through
    a Custom Property (e.g. from a Scene, Collection or Object
    data-block).
  - See
    blender/blender#106321
    and the related commits
    blender/blender@b3c7f3c8a9
    and
    blender/blender@f052b18a65.
- Keep some animation data from library overrides when no animation data
  existed originally in the linked reference, but it got added later on
  in the library file. Previously, any change related to animation done
  in the liboverride data would have been lost
  (blender/blender#110067,
  blender/blender!110900,
  blender/blender@f188d6709f).
