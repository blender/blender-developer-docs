# Blender 2.91: More Features

## UV/Image Editor

- Alpha blending are now done in Linear Scene Reference Space.
- Support of pure emissive colors.
- Improved performance; especially noticeable when viewing render
  results and repeating image.
- The smoothness of the UV's can now be controlled in the User
  preferences
  (blender/blender@b2fc0678542a)

## Sequencer

The FFmpeg library is now compiled with vectorization (SSE/AVX/AVX2)
support enabled, allowing for faster video decoding and encoding. In the
tests re-encoding 3405 2560x1376 frames on Xeon E5-2699 V4 CPU overall
time down from **313** seconds to **210** seconds ([sample
file](https://storage.googleapis.com/institute-storage/vse_simplified_example.zip)).

## Motion tracking

- Implemented Brown-Conrady distortion model, improving compatibility
  with other software
  (blender/blender@3a7d62cd1f5)

## Shaders

- Add Emission Strength to Principled BSDF shader, as a multiplier to
  the Emission color.
  (blender/blender@b248ec9)

## Cycles

- Motion blur rendering from Alembic files.
  (blender/blender@b5dcf746369e)
- Overriding the compute device from the command line.
  (blender/blender@cfa101c22871)
- NVIDIA RTX 30xx graphics cards support without runtime compilation.
- For AMD graphics cards, the latest Pro drivers must be installed.
  These contain critical fixes for Cycles OpenCL rendering to work.
