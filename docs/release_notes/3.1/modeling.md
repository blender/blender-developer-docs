### General

- Mesh vertex and face normals have been refactored, reducing
  unnecessary calculation, improving caching, and generally improving
  performance in more complex files
  (blender/blender@cfa53e0fbeed71).
  - These changes do not affect pure edit mode performance.
- Mesh bounding box calculation has been multi-threaded, which can make
  exiting edit mode faster
  (blender/blender@6a71b2af66cf10).
- The accuracy of NURBS knots calculation has been improved, allowing
  the combination of Bézier and cyclic modes
  (blender/blender@45d038181ae259).
- An new operator on meshes allows converting generic attributes to UV
  layers or vertex groups, or other generic data types
  (blender/blender@f6888b530ac81a).
  - The operator also makes it possible to change the domain of an
    attribute on original geometry.

### Subdivision Surface

- The Subdivision Surface modifier now benefits from GPU acceleration
  (blender/blender@eed45d2a239a).
  - To enable or disable the GPU acceleration, a new user preference
    setting is available under *Viewport \> Subdivision*.

<!-- -->

- Vertex creasing for subdivision surfaces is now supported
  (blender/blender@4425e0cd64ff).
  - This allows to mark vertices as arbitrarily sharp to create
    interesting shapes more efficiently:

![Example shapes made using vertex and edge creasing.](../../images/Vertex_crease_cycles.png)
