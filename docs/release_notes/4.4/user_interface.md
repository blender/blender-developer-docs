# Blender 4.4: User Interface

## Asset Browser

- New option to sort assets by asset catalog instead of by name.
  This is the new default sorting method.
  (blender/blender@f49d0390a1b6)

![Asset sort by catelog](images/AssetSortCatelog.png)

- New operator to remove the preview of an asset. Find it in the Asset Browser sidebar. (blender/blender@40b0185a8e)
- Brush names are easier to read with Light theme. (blender/blender@dcec1d5)

## Files

- Default file names now capitalized as "Untitled". (blender/blender@0ee4ae89e4)
- Full file name now shown on the Recent items tool-tips. (blender/blender@0e83b9c5ee)

![Recent item tooltips showing full name](images/RecentTipFileName.png)

## Fonts

- Interface font, "Inter", updated to 4.1. (blender/blender@6696f11808)
- Improved calculation of text string length for monospaced fonts. (blender/blender@7492c7aa6)
- Improved selection from Fonts folder. (blender/blender@7a1e95ea91)

## General

- Centered dialogs can now be moved without them snapping back. (blender/blender@154ab16ee2)
- Popup informational alerts, like Python script warning, adjust width based on contents. (blender/blender@0add2857a3)
- Menu accelerators (underlined letters) now work with toggles. (blender/blender@ba9417470e, blender/blender@99d028cd7d)

![Menu toggles with keyboard accellerators](images/ToggleAccellerators.png)

- _Adjust Last Operation_ becomes unavailable after undoable interactions with UI elements. (blender/blender@1bde901bf28e)
- Increased contrast for transform cursors. (blender/blender@26c15292a7)

![Improved transform cursor contrast](images/TransformCursors.png)

- Larger alternative version of the "frame" cursor for high-DPI displays. (blender/blender@1f33b401b3)
- Batch Rename: Add icons to data type menu. (blender/blender@e2e992a425)

![Batch rename icons](images/batch_rename_icons.png)

- Rename masking property to better reflect its usage. (blender/blender@18f145d1ac)
- Eyedropper color picking operations can now be canceled without losing the original color. (blender/blender@96babd5e9f)
- Preview images load faster and display with less artifacts on different sizes (blender/blender@315e7e04a8)
- Image Editor view mode now uses "sample" as default tool.  (blender/blender@15e367f31a)
- UV Editor now has Ctrl-C and Ctrl-V shortcuts for copying and pasting UVs.  (blender/blender@e95687796d)
- Can now set a selected outline color for pulldowns.  (blender/blender@be975efc9e)
- "Material" icon design flipped horizontally to better differenciate from "World".  (blender/blender@17c98a2c8c)
- A warning message is shown when drag and drop of images fail.  (blender/blender@5eb8101efa)
- Light theme icon border is now closer in darkness to prior Blender versions.  (blender/blender@05151fcd60)
- Reset to Defaults in Redo Panel no longer closes it.  (blender/blender@175d812797)
- Preview items show a loading icon while loading or being rendered. (blender/blender@1f88645728)
- Preview items show a slight background in menus. (blender/blender@4755503356)
- Color picker HSL value can now be adjusted by trackpad scrolling. (blender/blender@9692c762c5)
- Pie menus now close on loss of window focus. (blender/blender@b5b786b34f)
- Improvements to 3Dconnexion NDOF support. (blender/blender@c3d92f32dc)

## Status Bar
- Warning on Status Bar when active object has non-uniform or negative scale. (blender/blender@06a43d7d99)

![Warning when active item has non-standard scale](images/ScaleWarning.png)

- Warning on Status Bar when transform operation has no effect. (blender/blender@40eadc75be)

![Warning for invalid transform](images/TransformWarning.png)

- Color Picker Status Help shown for MacOS for picking outside of Blender. (blender/blender@8c9446a0d7)
- Improved Status Bar display for Mesh Point Normals. (blender/blender@8ad3653112)
- Improved Status Bar display for UV Stitch. (blender/blender@bd3bc46b42)
- Improved Status Bar display for Grease Pencil lines. (blender/blender@5a452884fc)
- Improved Status Bar display for "Annotation Draw". (blender/blender@0b38b3b2b1)
- Improved Status Bar display for “Add Marker at Click”. (blender/blender@8aafcc7f64)
- Improved Status Bar display for “Blend Pose Asset”. (blender/blender@efe75841d3)
- Improved Status Bar display at window and area edges. (blender/blender@b58ff3484f)
- Improved Status Bar display for header regions. (blender/blender@34c135c30)
- Improved Status Bar display for “Grease Pencil Fill”. (blender/blender@1a2b126d5c)
- Improved Status Bar display for Key Blending operators. (blender/blender@46a418e7b5)
- Improved Status Bar display for Pose Inbetweening. (blender/blender@f5a07e715f)
- Box select now shows status bar key for deselect. (blender/blender@a9b863be43)
- Bevel now uses "Profile Shape" name in header and in Status Bar. (blender/blender@3e11cfb1d0)
- Status Bar no longer shows Confirms and Cancels that are not when Release on Confirm. (blender/blender@867e9d879a)
- Status Bar notification banners are now truncated when very long. (blender/blender@0ba91e9870)
- Improved Status Bar display for Walk Navigation. (blender/blender@3723906e30)
- Improved Status Bar display for Fly Navigation. (blender/blender@02765b76cc)
- Context menus now shown as "Options" on the Status Bar. (blender/blender@fd97a8f578)

![Warning for invalid transform](images/WalkWASD.png)

## 3D Viewport

- Default color for front face for Face Orientation overlay changed to transparent.
  (blender/blender@8bcb714b9e)
- Overlays: Mesh indices overlay setting is now always visible, it no longer depends on Developer Extras. (blender/blender@ef2bff2004)
- Notification shown when hiding objects. (blender/blender@bc5fcbe1c3)
- Can delete "Measure" tool items when gizmos are turned off. (blender/blender@3f91350d4e)
- Using Text Objects with a non-default font, missing characters are no longer loaded from other fonts. (blender/blender@6fa5295c1d)
- Using Text Objects with a missing or invalid font, characters no longer used from default font. (blender/blender@5ce10722ab08)
- Can now play animation while in Sculpt mode. (blender/blender@1debbcae1e)
- Viewport Render Animation now shows a progress bar. (blender/blender@627114ee54c87bb5d9c243849996c3b81346d040)
- Expand brush/tool falloff curve presets in popovers (blender/blender@5e5c68fa62)
- Use gizmo theme colors for knife tool overlay (blender/blender@9d5a4756ca)
- Mesh indices are now more readable, similar style as Attribute Viewer (blender/blender@2536b04a21)
- NDOF Orbit direction is now inverted when upside down. (blender/blender@a2b5cb5863)
- Object data name show shown in text overlay. (blender/blender@55de54557c)
- FPS display in overlay no longer jiggles up and down when starting. (blender/blender@a7178e69f4)

## Editors

- Resizing editors now softly snaps to minimum and maximum.
  (blender/blender@62541bffc2)
- Improved rounded edge for splitting preview.
  (blender/blender@c3ea941273)
- Docking operation feedback descriptions improved.
  (blender/blender@09e6bab7bc)
- Minimum size imposed for docking operation creation of new editor.
  (blender/blender@f339ab4325)
- Splitting improvements including soft snapping and minimum size increased.
  (blender/blender@82667d5626)
- Using separate specific mouse cursors for Join operations.
  (blender/blender@798a48f6ef)
- Correct shortcuts now shown when hovering over Editor Change button. (blender/blender@9ed7b03e35)
- Scroll-bar properly hidden for tiny editors. (blender/blender@b846797074)

## Spreadsheet

- The selection filter for meshes now gives expected results for non-vertex domains.
  (blender/blender@76e9dc0f0424)

## Nodes

- The sidebar is see-through when *Region Overlap* is enabled in the Preferences, similar to the 3D Viewport (blender/blender@9b7e660fad).
- Panels in node groups can now be nested
  (blender/blender@7dc630069b48fa16ce1d4aedc1513dd5bdd6cdb7).
  ![](images/nested_group_node_panels.png)
- The name input in the Attribute node in the shader editor is wider now (blender/blender@ea8c45e10a154be22a9f7d47f5cbfb49ab368f84).
- Node title text truncation is improved (blender/blender@5edc68c442, blender/blender@4ff47d85e0).
- Long multicolumn lists in nodes will now collapse to single column when space limited. (blender/blender@fe071cd076)
- Input values that don't affect the output are grayed out now. (blender/blender@80441190c6e04ec2).
  ![](images/grayed_out_node_inputs.png)
- A color node is inserted when dropping a color into the node editor (blender/blender@22582bf6f0347f).

## Outliner

- Improved Outliner vertex group sorting. (blender/blender@531ed68061)
- Outliner supports ctrl/shift for excluding collections. (blender/blender@a1fc2eb37a)
- No more overlapping icons in Outliner with some display options. (blender/blender@25febbbb32)
- Can now un-isolate collection when a linked collection is present.
  (blender/blender@f181262634)
- Non-object active item text now drawn in "text high" color. (blender/blender@62a0350f6d)
- Drag and drop to scene now updates the view. (blender/blender@340aa78724)
- Child objects linked to other collections are now faded. (blender/blender@f4f2248a9c)

## Properties Editor

- Tree-views no longer forget their changed height and scroll offset after hiding/unhiding them,
  or reloading the file (if _Load UI_ is enabled). (blender/blender@f0db870822, blender/blender@ed0d01c5afda)
- Tree View displays lines correctly when local zoom changes. (blender/blender@5ec2547ab0)
- UI Lists now be sorted in reversed alphabetical. (blender/blender@72a9990779)
- Improved tab key navigation between inputs in the color picker. (blender/blender@93e8c231aa)
- Opening context menu from numerical input now shows correct mouse cursor.
  (blender/blender@e6d941cdf4)
- Custom integer properties no longer have -10000/10000 soft minimum and maximum.
  (blender/blender@9e6c884394)
- Improved container/codec ordering in FFMPEG video drop-downs. (blender/blender!118412)
- Reset to Defaults now works with Camera field of view. (blender/blender@e5e45dc140)
- Mesh Cache modifier "Flip Axis" toggles now work correctly. (blender/blender@d2418b89c3)

## Preferences

- More user preferences now reset to actual defaults. (blender/blender@6a06e87f85)
- Language translation options are now preserved when changing languages. (blender/blender@b801615431)
- Preference for UI Icon alpha no longer affects matcaps and studio lights. (blender/blender@2b7a968909)
- Minimum User Scale preference increased to 0.5. (blender/blender@6922418a95)
- Extensions: Add button to quickly access an add-on's folder. (blender/blender@6ea8297477)
- Improved Studio Lights Editor interface layout. (blender/blender@f6bbcaba6d)

![Studio Lights Editor Layout](images/studio_lights_editor_layout.png){ style="width:400px;" }

## Animation

- Animated values for nodes, node sockets, and geometry nodes inputs have more useful names in the list (blender/blender@bd8edc7c27513842b083fc0a481a4b9935dd13a6).
- Auto keyframe toggle now works better if assigned to keyboard shortcut. (blender/blender@0e1313483a)
- Improved feedback during Animation Playback timer test. (blender/blender@df57beb676)
- Improved current frame indicator styling in movie clip and image editors. (blender/blender!132911)

## Tooltips
- Tool-tips have improved vertical centering. (blender/blender@8dc8e48e9a)
- Tooltip status colors work better on light backgrounds. (blender/blender@c5bce9d710)
- Colorspace tooltips now show more information with expanded acronyms. (blender/blender@5bcde9cbff)
- Color tooltips now propertly show colors that are without alpha. (blender/blender@dbe50711ce)

![Color tooltips differ with alpha](images/ColorTipAlpha.png)

- Tooltips can now show while animation is playing. (blender/blender@a7334db09b)
- Tooltips are removed quicker with Gizmo interation. (blender/blender@e8bc7ec68b)
- Tooltips no longer redraw with minor mouse movement. (blender/blender@4450799673)

## Keymap
- UV Editor: Add shortcut to Copy/Paste UVs (Ctrl/Cmd+C, Ctrl/Cmd+V) (blender/blender@e95687796d)

## Platforms

- macOS: OS Title bars now use theme colors.
  (blender/blender@ce42d92503)

![macOS Colors](images/TitlebarMac.png)

- Windows 11: OS Title bars now use theme colors.
  (blender/blender@3f4a1cabbf)

![Windows 11 Colors](images/TitlebarWin11.png)

- Windows: Copy and paste OS image paths into Image Editor.
  (blender/blender@0193579e90)
- Windows: File system volume names display correctly with high-bit Unicode characters.
  (blender/blender@fe6609c4eb)
- Windows: AltGr key (used in some keyboard layouts) is now treated as regular Alt key.
  (blender/blender@d99d19cd22)
- macOS: Fixed title bar file icon not being cleared when creating a new file.
  (blender/blender@b99497bf21)
- macOS: Improved color picking outside of Blender windows.
  (blender/blender@60a3f886c6)

  ![macOS Color Picking](images/macos_color_picking_outside_windows.png)
  ![macOS Status Bar information about color picking](images/MacPickerHelp.png)

- macOS: Keymaps can be searched with native key names.
  (blender/blender@001c3b0d4b)

- macOS: Main window title improvements.
  (blender/blendler@901119cf8)

  ![macOS Window Title Improvements](images/macos_window_title_improvements.png)

- macOS: Preview blend file contents in a thumbnail in Finder, App Exposé and Spotlight.
  (blender/blender!107072, blender/blender@b858319dd33b466f3de5be76b72cabc306d4d9f8)

  ![Grease Pencil Fundamentals by Blender Studio thumbnails in Finder](images/macos_thumbnails_finder.png)
  ![Sprite Fright by Blender Studio thumbnail in Spotlight](images/macos_thumbnail.png)

- macOS: Enhanced NDOF device input handling when using 3DConnexion driver v10.8.7 and later.
  (blender/blender@c3d92f32dc3046bf95d73c864c98fb0ff0316146)

- Linux: Fixed text pasting from Blender to certain other applications (such as Firefox) not working under X11.
  (blender/blender@720fc44ca3)
