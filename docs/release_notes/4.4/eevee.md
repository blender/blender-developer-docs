# Blender 4.4: EEVEE & Viewport

## 3D Viewport

### Overlays

The overlays have been rewritten for better consistency and better extendability.

This is the cause of small changes in the way overlays are drawn:

- Objects origins now always draw on top of edit mode overlays.
- Display in Xray mode now fade in-front overlays correctly.
- The `Wire Color` option under the `Viewport Shading` popover is now honored by all wire objects in both `Solid` and `Wireframe` shading mode.
- Wireframes are now displayed in Edit Mode even when Overlays are off.  (blender/blender@5ccc02bbf4)

### User Interface
* Expose view *Lock Rotation* property in the sidebar View panel. (blender/blender@7d98a24e3e)

## Vulkan

The Vulkan experimental backend received a big update in performance, stability and compatibility.
The backend is still considered experimental due to missing features like OpenXR, OpenSubdiv,
Hydra viewport and animation performance.

### Performance

#### Startup Time

*Test*: Start Blender, open barbershop_interior.blend (3000 objects, 550 materials, 200 images),
wait for final viewport.

The next figures shows the compilation times in second using NVIDIA Quadro RTX 6000 on a AMD Ryzen 9 7950X (32-Core). 

For OpenGL the default behavior is to use no subprocesses.

```mermaid
xychart-beta horizontal
    title "Cold Start"
    x-axis ["OpenGL (no subprocesses)", "OpenGL (8 subprocesses)", "Vulkan"]
    y-axis "Time (in seconds)" 0 --> 120
    bar [112, 56, 30]
```

```mermaid
xychart-beta horizontal
    title "Warm Start"
    x-axis ["OpenGL (no subprocesses)", "OpenGL (8 subprocesses)",  "Vulkan"]
    y-axis "Time (in seconds)" 0 --> 10
    bar [6, 6, 3]
```

| Backend | Test                         | Duration |
| ------- | ---------------------------- | -------- |
| OpenGL  | Cold start (no subprocesses) | 1:47     |
| OpenGL  | Warm start (no subprocesses) | 0:06     |
| OpenGL  | Cold start (8 subprocesses)  | 0:56     |
| OpenGL  | Warm start (8 subprocesses)  | 0:06     |
| Vulkan  | Cold start                   | **0:30** |
| Vulkan  | Warm start                   | **0:03** |


#### Cycles Render

Vulkan is used for displaying the rendered result of Cycles. The
process has been reimplemented using a new threading model resulting
in the same performance compared to OpenGL.

### Compatibilty

Vulkan has been tested to work on the next platforms and drivers. 

| **Devices**                   | **Windows**           | **Linux**                  |
| ----------------------------- | --------------------- | -------------------------- |
| **NVIDIA**                    |                       |                            |
| GTX 700 - GTX 800             | NVIDIA 500+           | *Unsupported*              |
| GTX 900 - GTX 1000            | NVIDIA 500+           | NVIDIA 550+                |
| RTX 2000 - RTX 5000           | NVIDIA 500+           | NVIDIA 550+                |
| **AMD**                       |                       |                            |
| HD 7000 - HD 8000             | *Unsupported*         | Mesa                       |
| R 200, R 300                  | *Unsupported*         | Mesa                       |
| RX 400 - RX 600               | Latest AMD Official   | Latest AMD Official, Mesa  |
| RX Vega                       | Latest AMD Official   | Latest AMD Official, Mesa  |
| RX 5000 - RX 9000             | Latest AMD Official   | Latest AMD Official, Mesa  |
| **Intel**                     |                       |                            |
| Intel 6-10th gen iGPU         | *Unsupported*         | Mesa                       |
| Intel 11th gen iGPU and above | Latest Intel Official | Mesa                       |
| Intel Arc                     | Latest Intel Official | Mesa                       |
| **Qualcomm**                  |                       |                            |
| Adreno X1-85 GPU              | Upcoming driver       | *Not tested*               |

### Known Issues

- Windows + NVIDIA: Opening preferences twice leads to a crash (blender/blender#134928)

## Profiling

There's a new `--gpu-profile` command line argument that exports GPU and CPU profiling data ([Docs](https://developer.blender.org/docs/features/gpu/tools/perfetto/)).
