# Blender 4.4: Compositor

## Added

- Integer sockets are now supported in the compositor. (blender/blender@b3623feab2)
- Exposed the Quality option of OpenImageDenoise on the Denoise Node. (blender/blender@8efd41271d)
  - This allows users to make trade offs between quality and performance of denoising in the
    compositor, similar to what can already be done for Cycles renders.

## Changed

- The CPU compositor was rewritten to prepare for future development. The rewrite provides
  significant improvements in performance in certain configurations of some nodes, caching of static
  resources like images, and less memory usage on node setups with many pixel nodes. So the
  compositor should generally be faster. The following graph demonstrate the relative performance of
  some nodes in certain configurations.

![New CPU Compositor Relative Performance](images/newCPUCompositorRelativePerformance.png)

- The Fast Gaussian mode of the Blur node is now much more accurate.

![Old Fast Gaussian (Left) vs New Fast Gaussian (Right)](images/fastGaussianImprovement.png)

- Transformations are now delayed until it is necessary to apply them. So scaling down an image then
  scaling it up again will no longer pixelate it, that is, transformations are no longer destructive
  until the image is actually processed. (blender/blender@109478d599)
- The Wrapping option in the Translate node was turned into a Repeat option that infinitely
  repeats when mixed with a larger image. (blender/blender@5fd5685a6a)
  
![Example Repeating Using Translate Node](images/translateNodeRepeat.png)
  
- The Glare node was revamped to provide a better user experience and more flexible control:
  - Node options are now single value inputs that can be linked. (blender/blender@004e3d39fa)
  - The generated glare and the highlights are now exposed as outputs. (blender/blender@004e3d39fa)
  - A new Strength input now controls the strength of the glare, and can boost the glare power.
    (blender/blender@004e3d39fa)
  - Fog Glow size is now linear, relative to the image size, and can have any size.
    (blender/blender@004e3d39fa)
  - Bloom size is now linear and relative to the image size. (blender/blender@004e3d39fa)
  - Bloom now conserves energy better and has a more reasonable output range.
    (blender/blender@b92a6eab3a)
  - The saturation and tint of the glare can now controlled using the new Saturation and Tint
    single value inputs. (blender/blender@c37d51f73d)
  - The highlights can now be clamped and smoothed using the new Smoothness and Maximum single
    value inputs. (blender/blender@3b28cf276e)
  - The inputs are now organized into panels for compactness and clarity.
    (blender/blender@14a380089a)

![The new Glare node.](images/newGlareNode.png)

- Implicit conversion from a color to a float socket now computes the luminance instead of the average of the RGB channels. 

## Compatibility

- The Glare node options were turned into inputs, are now deprecated, and will be removed in a
  future version.
- The Fog Glow and Bloom modes of the Glare node were corrected due to the revamped Glare node, but
  the input image size was assumed to be the render size during correction, since guessing the real
  input size was difficult. If this was not the case, turning the Size and Strength inputs should
  restore the old result.
- If loaded node trees from previous versions contain a color to float link, two additional hidden nodes (Normal and Multiply) will be created to compute the average of the RGB channels instead of the luminance.
