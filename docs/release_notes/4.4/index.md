# Blender 4.4 Release Notes

Blender 4.4 is currently in **Beta** until March 12,
2025.
[See schedule](https://projects.blender.org/blender/blender/milestone/24).

Under development in [`blender-v4.4-release`](https://projects.blender.org/blender/blender/src/branch/blender-v4.4-release).

* [Animation & Rigging](animation_rigging.md)
* [Compositor](compositor.md)
* [Core](core.md)
* [Cycles](cycles.md)
* [EEVEE & Viewport](eevee.md)
* [Geometry Nodes](geometry_nodes.md)
* [Grease Pencil](grease_pencil.md)
* [Modeling & UV](modeling.md)
* [Pipeline & I/O](pipeline_assets_io.md)
* [Python API](python_api.md)
* [Sculpt, Paint, Texture](sculpt.md)
* [User Interface](user_interface.md)
* [Video Sequencer](sequencer.md)

## Compatibility

* Cycles: [Increased minimum driver versions](cycles.md#other) for AMD and Intel GPU rendering.
* Alembic and USD: [Processing of edge and vertex creases](pipeline_assets_io.md#alembic-and-usd) has changed for consistency with OpenSubdiv.
* Python API: [Breaking changes](python_api.md#breaking-changes) for actions, sequencer strips and subclassing Blender types.
* Libraries have been upgraded to match VFX platform 2025, including:
  * MaterialX 1.39
  * OpenColorIO 2.4
  * OpenEXR 3.3
  * OpenShadingLanguage 1.14
  * OpenUSD 25.02
  * OpenVDB 12.0
